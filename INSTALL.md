# INSTALL

## How to install npm?
```
> curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.4/install.sh | bash
> export NVM_DIR="/home/gitlab-runner/.nvm" && [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
> nvm install 6.9.1
> nvm use 6.9.1
> npm install gulp -g
```