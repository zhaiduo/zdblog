//http://webpack.github.io/docs/webpack-dev-server.html
const NODE_ENV = process.env.NODE_ENV;
const dotenv = require('dotenv');

const webpack = require('webpack');
const fs = require('fs');
const path = require('path'),
    join = path.join,
    resolve = path.resolve;
//https://www.npmjs.com/package/hjs-webpack/
const getConfig = require('hjs-webpack');

const CopyWebpackPlugin = require('copy-webpack-plugin');

const isDev = NODE_ENV === 'development';
const isPro = NODE_ENV === 'production';
const isTest = NODE_ENV === 'test';

const root = resolve(__dirname);
const src = join(root, 'src');
const modules = join(root, 'node_modules');
const dest = join(root, 'dist');

var config = getConfig({
    isDev: isDev,
    in : join(src, 'app.js'),
    out: dest,
    clearBeforeBuild: true,
    html: function(context) {
        return {
            'index.html': context.defaultTemplate({
                title: '窄多之Blog - Zhaiduo.com',
                publicPath: isDev ? 'http://localhost:3000/' : '',
                /*meta: {
                    'Cache-Control': 'max-age=0',
                    'Cache-Control': 'no-cache, no-store, must-revalidate',
                    'Pragma': 'no-cache',
                    'Expires': 'Tue, 01 Jan 1980 1:00:00 GMT',
                },*/
                //<meta http-equiv="Cache-Control" content="max-age=0"><meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"><meta http-equiv="Pragma" content="no-cache"><meta http-equiv="Expires" content="0"><meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
                html: '<meta http-equiv="Cache-Control" content="max-age=0"><meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"><meta http-equiv="Pragma" content="no-cache"><meta http-equiv="Expires" content="0"><meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT"><link rel="shortcut icon" href="//2018.zhaiduo.com/favicon.ico"><link rel="alternate" type="application/rss+xml" title="Zhaiduo Blog - RSS" href="https://2018.zhaiduo.com:8003/post/?_start=1&_limit=500&_sort=wp:post_date&_order=DESC&format=rss" /><link rel="stylesheet" href="zdblog.css"><script>var __ptLang=\'zh_cn\';var __ptLangArr={};</script></head><div class="body"><div class="slider-container slider-container-fullscreen"><div class="slider" id="revolutionSliderFullScreen"><ul><li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="6400" data-thumb=""><img src="/bg.jpg" alt=""></li></ul></div></div><div role="main" class="main zdPage zdPageMain"><div class="container "><div class=""><div class="logo"><a href="/"><img alt="2018.Zhaiduo.com" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAAHfgQuIAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAipSURBVHjaYvz//z8DLsASuucbiP5/f/WSCsXQmE4gWwnIvstw9iwDC1QRI1ACpgEswWBszMiEZhLIBAbFjn5GEIcJXQKo6yxMACaZBpUA6TKBip2B2TkTKgEyThCo8B2IDxBAjIS8AlP5HqhTCMluRsaQ3V8ZsDjqHpCtjOxaF5ijgH5URvfKbiRHoXgFxfPIkjB7ZsEUQjEDQAAhOwi3n6AAbMg3YERdv46wEmQIUMLkfkUh2EAkzPDp9o0PcI9cvw7yCEYQoAQH3Jb37xkY7t0DxzBYBikZYGh6ffLI1y+PHnAjhwrYEFiM4LTlxQtuhqdPMUIMm8b/z/ZsY/j5/h0DetgjGbqa4fnzciB9D+RHgqH579cvhocbV8ECjeHMzDQGgADCm7LwAVgGUgLnDTzg091bL96eOyXBAMkI8EQH0rQHFCjY4hEEkDS9B/kRFjgmQE1ngDmTkQGRO1FD+ds3iIixsRDIAJiNZ8GaUIExStwBkxk8AaBlAXRw5sO1S+9gKQUIhNBzAM7k9v7qJSGopnvAeH1PSCNy6gELADUpoytC1xiK4i88SQ5d4ypQ4ob5C5cmjKIORMByBDhdYgeCILVMGP4COg/qrzAkxTPh6s6eBYU0IygBlKP4CxggaE6EGZrGgCh3GYjKHUgZGJ47AAIIJXeYpM9C1wNyYhoDpQDNpchS76CBBgZfnzxkeHX8MIRz5QoDw8+fYHcBg+AsmpEdsKD6/+/f1wdrl3FjsQxU5VUglzh3oZa5AoNCCeo7E6AGUIkhCK1YcKZqEHi8bQPDn69fIJaBCtP372EZkRFmMQu8SmFgAIVnJ9AykKWh8MwZGqOMw6JVsIT9/+/f7w/WLefE4qsKoGWdyJrwFf/4ANxXoCITVHTCSwhE8cKILR6ZSLRoFcwyoCXvQKkQbhnIQIhlFfhyIgsFvhJCS0x4szwx5SK8fIRZ9ufb1xdwX/39C/EVxLIwYiwjxof/0TKxBDm+IsaHcF/9/vTxAbzEAFmC8JUJqZZh86EgNPMj+0oBzLlwARKMhH21ClpzpiOVVIxQD1QwoWV8sGU/Xr98guEriGX4fLUbaiioOg8Fs//8iQDrPXsWJO4KyuegfIjNVxhlIEbQCAkzyJbVoIjB20Yg8OYNMDk/hEmBCo974IxPbtOGXAAQgBjz10kgCML4QezFqyyo8BGktxAKE1usfAVreQSoLKx4BSmsrDgTY42RRIwmxGs0ooUsUkisdOdudvbP7YU9juhWELjM7X4z8/1m/zygljQWe8r/NvrRthPTxCp2kZIHkLn+mlES2prc3Xqfj/fqWwY8S+uLLIpWGKr/8c0jJazlSHTK6eYI3zBE7K3zYIElGGU4TCPcD0vRF2jkjKlZmug0G7jtAgZj6IkVrL8gpcg3hCd+DPol+gWsSjp9aAZ8EmfMz78WdZPGoc9ZvId2YyvIbWHAsFIMmHFfbNqytKLAfUu4fYpear9N6iZ3FjN+SvOmdsWDVbN4I6A91w33w6TjG5ifxQ9tuompbBKNBMmsBJZhqwio6cad37foBrNIO4/jW3XDeTOhm22GWTYg6caGgznBk4NuywTsS92+p9OHYVwC4JFStwNzrFs2YENMqPFRdmVxAwWIqbZ10s3DNNas1JoygJTUreqqS9FVNxo5caZbNbVpugGPRqOq0A2nT9AtaxEXXXR7vjjftOgWuOrmEtCumwy2qM9mCvijUhgtOEY3LjU590zZRMcEYTJhYFOOfGViU7wNiYbUxYlWUMx5C4G4IW4VxDpGm4rW+KpXtpRAN8Ub1WBN5TPjPRaC1ZDEaSBNh2E3xKfn+bN1pHATpLVL6UkO3Tri+bfrS4+CzWYagaqOT7p9vb6EXLcKQZDUDa7+NIRc39n1/L19LfL8fSy/jEYqHt5kuuBxWXiDIQ04hidPXAj9G+r/CtCs1fM2EQTRK05CihHIUQpLUCALpEiUvp/gv+BU1LilC38hHbV7qkR8CRGEgQIqJK4BIQrsCyIBuYiyJAai2MKwcze3nr29r71bOzeNpbuTve+ed+bNm9X+QcfpZj1ixr0yE70sm6Yyix17X6xD9138Tfhvz+uJKNHAh20CHPS8YAP6CZFvjDNvGN1UvqnGd7KXQ8i0ohehOHJwDeVpyBAg37Ayk9gI/d4kgJtJ4MZ7w7Ofnz9eIBsOfmDAldR1It1C/2srJ0EdWg9p4NimocNalgu1iWartFi0NYekmjD/TblukxjbDHtIV6NWvqcVKozprzE72H0M92u6rKUBvE3AMfRxXKw4IbgAROeWy3N5m4BzNIAlvUg/wPHmf/l6btZaLSfJUqIApcXSYsLB9RFcOBmGQlXHBfY4sK4GsDys5d1rDgeX+lKptugjqO4Ck2Eia5JBRwMYIwogFFUcmKq+UxhkOWRfmUhkDWSopAzniStgDbWUDmtlvYQirB3FgQPWYsEBayCBZXDbvmDUAJdWJirHmmYCWzjArL12Ne9ei8xZzx1gE1lTMuHp6AcbvX1dz8kaQ9a8sgsyCVBMYqUecTb7ffD8SY0rHxUcHZzOAyTdXVOLshfJmjjwEQ08yBMJY6yZBJjFWuM8WDMBsNKslQVYedaKAtRnDZyZuVki4CJrzFpC2GVYm02nh/vPHq7NJhMVHD2EUaxHLCsyQFt7WQBbFrGdacB8iX36sKbcCA8hmmNNLBYbAfod4XGaHmkSqBOxkQZQnMWRWJtMjvZ3H62KgyWLZU1aLAFHrwcWXnCIqUOSF3jAzC7A2uoSWFMWi/ZwH/vWII6P71mDQVQWehaZLtsVYq1tkWl1kJr/fd3buf/CIta0CEheJyd3IldfoguhJBl91uSjDaLl1BlwpYlysK1Hb15dizW/4l9qL65Zh44+lrW/p3++f3v64IqJlHZ5/ab/eal5w7JrF4t7oMnWRXAuL6ajN+bdGwzJ91f6p/aK1pctfVaw7PgPv5oiGRdUAMIAAAAASUVORK5CYII="></a><span class="slogan">zhaiduo.com</span></div></div><section class="panel zdPanel"><header class="panel-heading text-center"><p>Loving Coding & Visual Design</p></header><div class="panel-body" id="app"><div class="fade in  text-center"><p><h1><a href="/about"><i class="fa fa-user-secret"></i> About</a><a href="/resume"><i class="fa fa-barcode"></i> History</a><a href="//zhaiduo.github.io/zdblog-hexo/""><i class="fa fa-beer"></i> Blog</a></h1></p><p class="zdIcon"><a href="javascript:void(0);" class="zdNoborder" rel="tooltip" title="zhaiduo at google.com"><span><i class="fa fa-envelope"></i></span></a><a href="javascript:void(0);" class="zdNoborder" rel="tooltip" title="４６９１６５９７"><span><i class="fa fa-qq"></i></span></a><a href="https://github.com/zhaiduo" class="zdNoborder" rel="tooltip" title="https://github.com/zhaiduo"><span><i class="fa fa-github"></i></span></a><a href="javascript:void(0);" class="zdNoborder" rel="tooltip" title="adamgogogoo"><span><i class="fa fa-weixin"></i></span></a><a href="https://www.linkedin.com/in/zhaiduo" class="zdNoborder" rel="tooltip" title="zhaiduo"><span><i class="fa fa-twitter"></i></span></a><a href="https://twitter/zhaiduo" class="zdNoborder" rel="tooltip" title="zhaiduo"><span><i class="fa fa-linkedin"></i></span></a></p></div><div><div id="container"></div><br><br><br> <ins class="adsbygoogle"></ins><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div></div><footer class=""><div class="info row"></div><div class="link"></div></footer></section></div></div></div>'
            })
        }
    }
});

// ENV variables
const dotEnvVars = dotenv.config();
const environmentEnv = dotenv.config({
    path: join(root, 'config', `${NODE_ENV}.config.js`),
    silent: true,
});
const envVariables =
    Object.assign({}, dotEnvVars, environmentEnv);

const defines =
    Object.keys(envVariables)
    .reduce((memo, key) => {
        const val = JSON.stringify(envVariables[key]);
        memo[`__${key.toUpperCase()}__`] = val;
        return memo;
    }, {
        __NODE_ENV__: JSON.stringify(NODE_ENV)
    });

config.plugins = [
    new webpack.DefinePlugin(defines),
    new CopyWebpackPlugin([{
        from: 'src/bg.jpg'
    },{
        from: 'src/css/zdblog.css'
    }])
].concat(config.plugins);
// END ENV variables

// CSS modules
const cssModulesNames = `${isDev ? '[path][name]__[local]__' : ''}[hash:base64:5]`;

const matchCssLoaders = /(^|!)(css-loader)($|!)/;

const findLoader = (loaders, match) => {
        const found = loaders.filter(l => l && l.loader && l.loader.match(match))
        return found ? found[0] : null;
    }
    // existing css loader
const cssloader =
    findLoader(config.module.loaders, matchCssLoaders);

const newloader = Object.assign({}, cssloader, {
    test: /\.module\.css$/,
    include: [src],
    loader: cssloader.loader.replace(matchCssLoaders, `$1$2?modules&localIdentName=${cssModulesNames}$3`)
})

/*const newloader2 = Object.assign({}, newloader, {
    test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    // Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
    // loader: "url?limit=10000"
    loader: "url"
}, {
    test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
    loader: 'file'
});*/

config.module.loaders.push(newloader);
cssloader.test = new RegExp(`[^module]${cssloader.test.source}`)
cssloader.loader = newloader.loader

config.module.loaders.push({
    test: /\.css$/,
    include: [modules],
    loader: 'style!css'
})
config.module.loaders.push({
    test: /\.(png|jpg|webp|gif)$/,
    loader: "url-loader?limit=50000&name=[name].[ext]",
    //query: { mimetype: "image/png" }
})
/*config.module.loaders.push({　　　　　　
    test: /\.html$/,
    loader: 'html-withimg-loader'　　　　
})*/

// CSS modules

// postcss
config.postcss = [].concat([
    require('precss')({}),
    require('autoprefixer')({}),
    require('cssnano')({})
])
// END postcss

// Roots
config.resolve.root = [src, modules]
config.resolve.alias = {
    'css': join(src, 'styles'),
    'containers': join(src, 'containers'),
    'components': join(src, 'components'),
    'utils': join(src, 'utils'),

    'styles': join(src, 'styles')
}
// end Roots

// console.log(require('prettyjson').render(config));



// Testing
if (isTest) {
    config.externals = {
        'react/addons': true,
        'react/lib/ReactContext': true,
        'react/lib/ExecutionEnvironment': true,
    }
    config.module.noParse = /[/\\]sinon\.js/;
    config.resolve.alias['sinon'] = 'sinon/pkg/sinon';

    config.plugins = config.plugins.filter(p => {
        const name = p.constructor.toString();
        const fnName = name.match(/^function (.*)\((.*\))/)

        const idx = [
            'DedupePlugin',
            'UglifyJsPlugin'
        ].indexOf(fnName[1]);
        return idx < 0;
    })
}
// End Testing

module.exports = config;
