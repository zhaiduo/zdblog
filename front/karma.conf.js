var path = require('path');
var webpackConfig = require('./webpack.config');
var argv = require('yargs').argv;

module.exports = function(config) {
    config.set({
        frameworks: ['mocha', 'chai'],
        webpack: webpackConfig,
        webpackServer: {
            noInfo: true
        },
        basePath: '',
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        browsers: ['PhantomJS'],
        concurrency: Infinity,
        reporters: ['spec'],
        files: [
            'tests.webpack.js'
        ],
        preprocessors: {
            'tests.webpack.js': ['webpack', 'sourcemap']
        },
        plugins: [
            'karma-mocha',
            'karma-chai',
            'karma-webpack',
            'karma-phantomjs-launcher',
            'karma-spec-reporter',
            'karma-sourcemap-loader'
        ],
        singleRun: !argv.watch
    });
};