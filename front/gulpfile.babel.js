//https://github.com/gulpjs/gulp/blob/4.0/docs/API.md
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

//cnpm install --save-dev gulp-clean gulp-rename gulp-if yargs chalk gulp-change when gulp-markdown-github-style merge-stream fs q
const version = process.env.npm_package_version;
//const version_date = process.env.npm_package_description;
console.log("Version: ", version);
//console.log("Version-date: ", version_date);
//const path = require('path');
//const clean = require('gulp-clean');
//const rename = require('gulp-rename');
//const exec = require('child_process').exec;
//const gulpif = require('gulp-if');
//const argv = require('yargs').argv;
//const chalk = require('chalk');
//const change = require('gulp-change');
//const when = require('when');
//const sequence = require('when/sequence');
//const markdown = require('gulp-markdown-github-style');

//const merge = require('merge-stream');
//const fs = require('fs');

//http://www.ituring.com.cn/article/54547
//const Q = require('q');

const compressBeauty = true; //false;

//sync css, font to www
gulp.task('ttf', function() {
  return gulp.src('./dist/*.ttf').pipe(gulp.dest("./www"));
});
gulp.task('eot', function() {
  return gulp.src('./dist/*.eot').pipe(gulp.dest("./www"));
});
gulp.task('woff', function() {
  return gulp.src('./dist/*.woff').pipe(gulp.dest("./www"));
});
gulp.task('woff2', function() {
  return gulp.src('./dist/*.woff2').pipe(gulp.dest("./www"));
});
gulp.task('svg', function() {
  return gulp.src('./dist/*.svg').pipe(gulp.dest("./www"));
});
gulp.task('jpg', function() {
  return gulp.src('./dist/*.jpg').pipe(gulp.dest("./www"));
});
gulp.task('png', function() {
  return gulp.src('./dist/*.png').pipe(gulp.dest("./www"));
});
gulp.task('css', function() {
  return gulp.src('./dist/*.css').pipe(gulp.dest("./www"));
});

gulp.task('copy', gulp.parallel('ttf', 'eot', 'woff', 'woff2', 'svg', 'jpg', 'png', 'css'))

gulp.task('www', function() {
  browserSync.init({
    server: {
      baseDir: "./www/",
      directory: false,
      index: "index.html"
    },
    port: 3030,
  //startPath: "/?preview=1"
  });
//browserSync.reload
});

////////////////////////////
gulp.task('default', gulp.series((done) => {
  // task code here
  done();
}));
