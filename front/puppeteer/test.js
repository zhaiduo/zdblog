//by zhaiduo@gmail.com @2017
//https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md
const fs = require('fs')
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
let iPhone6 = null;
const useBrowser = true;

const frontendHost = 'http://localhost';
const backendHost = 'http://localhost';
const backendHostSSL = 'https://localhost';
const frontendPort = 3000;
const backendPort = 8003;
const YOUR_DOMAIN = 'zhaiduo.com';
const YOUR_DOMAIN_HTTP = `https://${YOUR_DOMAIN}`
const styleSheetMatchReg = new RegExp("<link rel=\"stylesheet\" href=\"http: \\/\\/localhost:3000\\/[0-9\\.]+\\.hot\\-update\\.js\">", "gi");
const adsHtml = ' < div class="link" ></div > <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-0530210725070236" data-ad-slot="5122926539" data-ad-format="auto" data-full-width-responsive="true"></ins> <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>';

const sortIds = (renderIds) => {
  return new Promise((resolve)=>{
    //console.log("renderIds==", renderIds)
    renderIds.sort(function(a, b) {
      return a - b;
    });
    setTimeout(()=>{
      //console.log("renderIds", renderIds)
      resolve(renderIds)
    }, 1000)
  })
  
}

(async () => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--hide-scrollbars']
  });
  const page = await browser.newPage();
  if (!useBrowser) {
    iPhone6 = devices['iPhone 6']
    await page.emulate(iPhone6);
  } else {
    page.setViewport({
      width: 1250,
      height: 3020,
      //isMobile: true,
      //hasTouch: true,
      //deviceScaleFactor: 2
    })
  }

  page.waitForNavigation({
    waitUntil: 'networkidle',
    networkIdleInflight: 0,
    networkIdleTimeout: 5000
  })
  let idsDone = [];
  const timeout = (ms, data) => {
    return new Promise(resolve => setTimeout(resolve(data), ms));
  }

  const renderHtml = async (page, pageNum) => {
    let htmlName = (!pageNum) ? 'index.html' : pageNum + '.html'
    if (pageNum) {
      console.log(`${frontendHost}:${frontendPort}/${pageNum}`)
      await page.goto(`${frontendHost}:${frontendPort}/${pageNum}`);
    } else {
      await page.goto(`${frontendHost}:${frontendPort}`);
    }

    // Get the "viewport" of the page, as reported by the page.
    const dimensions = await page.evaluate(() => {
      //window.scrollBy(0, window.innerHeight - 1900);
      window.scrollBy(0, 900);
      return {
        width: document.body.clientWidth,
        //height: document.documentElement.clientHeight,
        height: document.body.clientHeight,
        deviceScaleFactor: window.devicePixelRatio
      };
    });
    //console.log('Dimensions:', dimensions);

    //await page.goto(`data:text/html,${html}`, { waitUntil: 'networkidle' });
    await page.screenshot({
      path: 'puppeteer/temp.png',
      fullPage: true
    });

    const saveHtml = (fs, fname, html, ids) => {
      return new Promise((resolve, reject) => {
        fs.writeFile(__dirname + '/' + fname, html, (err) => {
          if (err) {
            console.log('saveHtml err', err);
            //reject(ids)
            resolve(ids)
          } else {
            resolve(ids)
          }
        })
      });
    }

    const htmlTail = (n) => {
      return n + '.html'
    }

    const saving = async (page, htmlName, fn) => {
      let html = await page.content()
      const hostUrl = `<script src="${frontendHost}:${frontendPort}/app.js"></script>`
      html = html.replace(hostUrl, "")
      html = html.replace(styleSheetMatchReg, "")

      let ids = [];

      let nextId,
        prevId;
      if (html.match(/<link rel="next" id="([0-9]+)">/i)) {
        nextId = parseInt(RegExp.$1, 10);
        if (!ids.includes(nextId) && !idsDone.includes(nextId)) ids.push(nextId)
      }
      if (html.match(/<link rel="prev" id="([0-9]+)">/i)) {
        prevId = parseInt(RegExp.$1);
        if (!ids.includes(prevId) && !idsDone.includes(prevId)) ids.push(prevId)
      }

      html = html.replace('<button>下一页</button>', `<a class="next-button" href="${YOUR_DOMAIN_HTTP+'/'+htmlTail(nextId)}">下一页</a>`)

      html = html.replace(/<a href="\/([0-9]+|about|resume|admin)">/gi, (replacement, p1) => {
        //'<a href="/$1.html">'
        //console.log("replacement", replacement, p1)
        if (!ids.includes(p1) && !idsDone.includes(p1) && p1.match(/^[0-9]+$/)) ids.push(parseInt(p1))
        return `<a href="/${p1}.html">`
      })

      html = html.replace(/(integrator\.js\?domain=localhost)/gi,`integrator.js?domain=${YOUR_DOMAIN}`)
      //<div class="link"></div>
      //<ins class="adsbygoogle"></ins>
      html = html.replace('<div class="link"></div>', adsHtml)

      console.log('html:', nextId, prevId, html.length, htmlName);
      return await saveHtml(fs, fn, html, ids)
    }

    //const fn = `${__dirname}/../www/${htmlName}`;
    const fn = `../www/${htmlName}`;

    if (fs.existsSync(fn)) {
      console.log("exist fn", fn)
      return await timeout(100, [])
    } else {
      console.log("not exist fn", fn)
      return await saving(page, htmlName, fn)
    }

    /*fs.stat(fn, (err, stat) => {
      console.log("stat", err, stat)
      if (err == null) {
        //Exist
        return await timeout(100)
      } else if (err.code == 'ENOENT') {
        // NO exist
        return await saving(page, htmlName, fn)
      }
    });*/

  }

  //if isFullGenerate is not true, only generate html with limit number.
  const isFullGenerate = false;

  let pageNum = '';
  let renderIds = [];
  let tempIds = await renderHtml(page, pageNum);
  renderIds = renderIds.concat(tempIds);
  //console.log("tempIds", renderIds)

  //tempIds = await renderHtml(page, 'about');
  //tempIds = await renderHtml(page, 'resume');
  //tempIds = await renderHtml(page, 'admin');

  //generate html by ms with limit
  const myInterval = async (ms = 1000, limit = 0) => {
    let i = 0;

    while (Array.isArray(renderIds) && renderIds.length) {
      renderIds = await sortIds(renderIds)
      timeout(ms, true)
      const id = renderIds.pop()
      console.log("id", id)
      if (!idsDone.includes(id)) {
        if (id.toString().match(/^[0-9]+$/i)) idsDone.push(id)
        let tempIds2 = await renderHtml(page, id);
        renderIds = renderIds.concat(tempIds2);
        //console.log("tempIds2", tempIds2)
      }
      i++;
      //if (!isFullGenerate && i > 1) break;
      if (!isFullGenerate && typeof limit === 'number') {
        if (limit <= 0) {
          break;
        } else {
          limit--;
        }
      }
    }
    if (renderIds.length === 0) timeout(100);

  }

  await myInterval(ms = 2000, limit = 10)
  /*let ti = setInterval(async () => {
    if (Array.isArray(tempIds) && tempIds.length) {
      const id = tempIds.pop()
      console.log("id", id)
      let tempIds2 = await renderHtml(page, id);
      console.log("tempIds2", tempIds2)
      clearInterval(ti)
    } else {
      clearInterval(ti)
    }
    //timeout(5000)
  }, 10000)*/


  await browser.close();
})();

