import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'

import ReactDOM from 'react-dom'
import { combineReducers, applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import MyContainerComponent from '../../components/MyContainerComponent.jsx'
import store from '../../store.jsx'
import {todoApp} from '../../action.jsx'

import App from './App'
import styles from '../../styles.module.css'

describe('<App />', function () {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />)
  })

  it('has a single wrapper element', () => {
    //console.log('wrapper', wrapper)
    expect(wrapper.find(`.${styles.wrapper}`)).to.have.length(1);
  });
  it('has a Router component', () => {
    expect(wrapper.find('Router')).to.have.length(1);
  });
});

