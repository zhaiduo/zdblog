import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Router } from 'react-router';
import { combineReducers, applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import MyContainerComponent from '../../components/MyContainerComponent.jsx'
import store from '../../store.jsx'
import {todoApp} from '../../action.jsx'
import styles from '../../styles.module.css'


/*const App = React.createClass({
  render: function() {
    return (
      <Provider store={store}>
        <MyContainerComponent className={styles.wrapper}  />
      </Provider>
    )
  }
});*/

class App extends React.Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  // class getter
  get content() {
    return (<Router
        routes={this.props.routes}
        history={this.props.history} />)
  }

  //
  render() {
    return (
      /*<Provider store={store}>*/
        /*<MyContainerComponent className={styles.wrapper}  >

        </MyContainerComponent>*/
        <div className={styles.wrapper}>
          <div>{this.content}</div>
        </div>
      /*</Provider>*/
    )
  }
}

module.exports = App;