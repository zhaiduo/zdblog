import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Router } from 'react-router';
import { combineReducers, applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import MyContainerComponent from '../components/MyContainerComponent.jsx'
import store from '../store.jsx'
import {todoApp} from '../action.jsx'
import styles from '../styles.module.css'


import Navigator from './Navigator'

class Home extends React.Component {

  componentWillMount() {
    this.setState({}); // Just trigger a render
  }
  //
  render() {
    //console.log('constructor', this);
    return (
      <Provider store={store}>
        <div>
            <Navigator></Navigator>
            <MyContainerComponent className={styles.wrapper} pid={this.props.params.pid}  >

            </MyContainerComponent>
            
        </div>
      </Provider>
    )
  }
}

module.exports = Home;


