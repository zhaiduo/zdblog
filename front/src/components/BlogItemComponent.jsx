import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import store from '../store.jsx'
import {todoApp} from '../action.jsx'
import request from 'axios';

import {JWT,localData} from './JWT'
//const API_URL = 'http://zhaiduo.com:8080';//http://localhost:8080';
//const API_URL = 'http://zhaiduo.com:8003';//http://localhost:8080';
//http://localhost:8080/post/1

// Create the component that you want to make smart.
class BlogItemComponent extends React.Component {

  doAction(e) {
    //console.log('doAction', store);
      store.dispatch({
          type: 'SOME_ACTION',
          data: 'clicked'
      });
  }

  tidyData(data) {
    //console.log('tidy', data);
    if(typeof data !== 'string'){
      if(typeof data === 'object' && data.hasOwnProperty('$t')){
        var tmp = data.$t;
        return tmp.replace(/[\n]/ig, "<br>");
      }
    }
    return data;
  }

  tidyContent(data) {
    //console.log('tidyContent', data);
    if(typeof data === 'object' && data.hasOwnProperty('content:encoded')){
        return this.tidyData(data['content:encoded']);
    }
    return data;
  }

  componentWillMount() {

  }

  render() {
    //console.log('render', this.props, store.getState());
    //let data = store.getState();
    //console.log('store.getState', this.props);
    let html = this.props.blogData?this.tidyContent(this.props.blogData):'empty';
      // And you have access to the selected fields of the State too!
      // <p><b>{this.props.blogData.title?this.tidyData(this.props.blogData.title):'No Title'}</b></p>
      return <div>

      <div  dangerouslySetInnerHTML={{__html: html}}></div>
      </div>;
  }
}

// Select the props which you want to inject in the component,
// given the global state.
function select(state) {
    //console.log('blog item select', state, state.blog.data);
    return {
        data: state.blog.data
    };
}

// Wrap the component to inject the Dispatcher and the State into it.
export default connect(select)(BlogItemComponent);

