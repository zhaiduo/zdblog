import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import {Link} from 'react-router'
import request from 'axios';

import styles from '../styles.module.css'
import 'font-awesome/css/font-awesome.css'

import {JWT,localData} from './JWT'
import Navigator from './Navigator'
import Editor from './Editor'

const YOUR_DOMAIN = 'zhaiduo.com';

let jwtToken;
let currentToken = '';
let reqToken = new Promise((resolve, reject) => {
   let jwtToken;
   if(localData.getLocalData('jwtToken', true)){
      //console.log('token', localData.getLocalData('jwtToken', true));
      jwtToken = localData.getLocalData('jwtToken', true);
      var tokenObj = JWT.decodeToken(jwtToken);
      //console.log('decodeToken', tokenObj);
      if(tokenObj !== null){
        resolve(jwtToken);
      }else{
        reject(jwtToken);
      }

    }else{
      /*JWT.getNewToken().then(token => {
        resolve(token);
      }).catch(token => {
        reject(null);
      });*/
      reject(null);
    }
});
reqToken.then(token => {

  return new Promise((resolve, reject) => {
    var tokenObj = JWT.decodeToken(token);
    //console.log('tokenObj==', tokenObj);
    //console.log("jwtToken isSoonExpired", JWT.isSoonExpired(tokenObj.exp));
    //console.log("getTokenExpirationDate", JWT.getTokenExpirationDate(token), JWT.decodeToken(token))
    if(JWT.isTokenExpired(token)){
      //console.log("jwtToken isTokenExpired", JWT.isTokenExpired(token));
      //if outdate get new token
      JWT.getNewToken().then(token => {
          //jwtToken = token; //JWT.decodeToken(token);
          //resolve(token)
          reject(token)
        }).catch(token => {
          //console.log("failed get new token");
          reject(token)
        });
    }else{
      //console.log("jwtToken not isTokenExpired", JWT.isTokenExpired(token));
      //console.log("jwtToken isSoonExpired", JWT.isSoonExpired(tokenObj.exp));
      if(JWT.isSoonExpired(tokenObj.exp)){
        JWT.refreshToken({}, token).then(token => {
          currentToken = token;
          resolve(token)
        }).catch(token => {
          //console.log("failed update token");
          reject(token)
        });
      }else{
        currentToken = token;
        resolve(token)
      }
    }
  });
}).then(token => {
  //console.log("current token", JWT.getTokenExpirationDate(token), JWT.decodeToken(token))
  currentToken = token;
}).catch(token => {
  //console.log("jwtToken failed", token);
});

const getLastId = () => {
  return new Promise((resolve, reject) => {
    request
    .get(JWT.apiUrl() + '/post/?_sort=id&_order=DESC&_limit=1')
    .then(function(req) {
        //console.log('req', req.data[0].id);
        resolve(req.data[0].id)
    })
    .catch(function(req) {
        reject(null)
    });
  });

}
let newId = 0;
getLastId().then(n=>{
  newId = parseInt(n, 10)+1;
  //console.log('newId', newId);
});

class Admin extends React.Component {

  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.update = this.update.bind(this);
  }
  state = {
    token: '',
    postData : {}
  }
  login(event) {
    //console.log("login", event.target.value, this.username.value, this.passwd.value)
    let self = this;
    request
        .post(JWT.apiUrl() + '/auth/', {
          username: this.username.value,
          passwd: this.passwd.value,
        }, {
          timeout: 10000,
          headers: {
            //'X-Custom-Header': 'foobar',
            //'Authorization': 'JWT ' + currentToken
          }
        })
        .then(function(req) {
            //console.log('req', req.data);
            let p = localData.setLocalData('jwtToken', req.data.token, true);
            currentToken = req.data.token;
            /*store.dispatch(self.nextPost(req.data));
            self.getPosts(10, id);
            */
            setTimeout(function(){
              self.setState({}); // Just trigger a render
            }, 100);
        })
        .catch(function(req) {
            //store.dispatch(self.nextPostError(id));
        });
  }

  update(event) {
    //console.log("this.token", currentToken)
    //console.log("update", event.target.value, this.title.value, document.querySelector('#u_editor').innerHTML)

    if(!this.title.value || !document.querySelector('#u_editor').innerHTML){
      alert("Please enter content!");
    }
    //return false;

    this.state.postData = {
          //id:5,
          "title": {
            "$t": this.title.value
          },
          "link": {
            "$t": `https://${YOUR_DOMAIN}/?page_id=${newId}`
          },
          "pubDate": {
            "$t": new Date() //"Tue, 14 Nov 2006 16:29:48 +0000"
          },
          "dc:creator": {
            "$t": "admin"
          },
          "guid": {
            "isPermaLink": "false",
            "$t": `https://${YOUR_DOMAIN}/?page_id=${newId}`
          },
          "description": {},
          "content:encoded": {
            "$t": document.querySelector('#u_editor').innerHTML
          },
          "excerpt:encoded": {},
          "wp:post_id": {
            "$t": newId
          },
          "wp:post_date": {
            "$t": JWT.getLocalTime() //"2006-11-15 00:29:48"
          },
          "wp:post_date_gmt": {
            "$t": JWT.getLocalTime() //"2006-11-14 16:29:48"
          },
          "wp:comment_status": {
            "$t": "open"
          },
          "wp:ping_status": {
            "$t": "open"
          },
          "wp:post_name": {
            "$t": "%e7%a7%91%e6%8a%80%e6%96%b0%e7%9f%a5"
          },
          "wp:status": {
            "$t": "publish"
          },
          "wp:post_parent": {
            "$t": "0"
          },
          "wp:menu_order": {
            "$t": "0"
          },
          "wp:post_type": {
            "$t": "page"
          },
          "wp:post_password": {},
          "wp:is_sticky": {
            "$t": "0"
          }
        };
    //console.log("postData", this.state.postData)
    //return false;
    request
        .post(JWT.apiUrl() + '/post/', this.state.postData, {
          timeout: 10000,
          headers: {
            //'X-Custom-Header': 'foobar',
            'Authorization': 'JWT ' + currentToken
          }
        })
        .then(function(req) {
            console.log('req', req);
            document.location.reload(true);
            document.location.href='/';
        })
        .catch(function(req) {
            //store.dispatch(self.nextPostError(id));
        });
  }

  get_token() {
    request
        .get(JWT.apiUrl() + '/get-token/', {
          timeout: 10000,
          headers: {'X-Custom-Header': 'foobar'}
        })
        .then(function(req) {
            //console.log('get req', req);

            this.setState({
              token: req.data.token
            });

            //console.log("this.token=", req.data.token, this.state)

            /*store.dispatch(self.nextPost(req.data));
            self.getPosts(10, id);
            setTimeout(function(){
              self.setState({}); // Just trigger a render
            }, 100);*/
        })
        .catch(function(req) {
            //store.dispatch(self.nextPostError(id));
        });
  }
/*<p><textarea placeholder="your blog content" name="content" ref={(input) => { this.content = input; }} id="content" class="" cols="50" rows="6" ></textarea></p>*/
  render() {
    console.log("currentToken", currentToken)
    if(currentToken){
      return (
        <div>
        <Navigator></Navigator>
        <div className="fade in  text-center"  >

          <p>
              <strong>Blog Editor</strong>
          </p>

          <div className="fade in  text-center main">

              <p><input type="text" placeholder="Title" name="title" ref={(input) => { this.title = input; }} id="title" class="" /></p>

            <Editor id="u-editor" className="f-editor"></Editor>

            <p><input type="button" name="add" id="add" class="" value="Submit" onClick={this.update} /></p>

              <p className="zdMgTp40"><Link to="/"><span>Home</span></Link></p>

          </div>
        </div>
        </div>
      )
    }else{
      return (
        <div>
        <Navigator></Navigator>
        <div className="fade in  text-center"  >

          <p>
              <strong>Blog Editor</strong>
          </p>

          <div className="fade in  text-center main">

              <p><input type="text" placeholder="Name" name="username" ref={(input) => { this.username = input; }} id="username" class="" /></p>

            <p><input type="password" placeholder="EntryCode" name="passwd" ref={(input) => { this.passwd = input; }} id="passwd" class="" /></p>

            <p><input type="button" name="login" id="login" class="" value="Submit" onClick={this.login} /></p>

              <p className="zdMgTp40"><Link to="/"><span>Home</span></Link></p>

          </div>
        </div>
        </div>
      )
    }

  }
}

module.exports = Admin;