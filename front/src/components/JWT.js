
import request from 'axios';
class JWT {
    static apiUrl() {
        return document.location.href.toString().match(/https?:\/\/localhost/i) ? 'http://localhost:8003' : 'https://zhaiduo.com:8003'
    }
    /**
     * @function urlBase64Decode
     * @memberOf app.jwt
     * @description base64 decode
     * @param  {string}  str       [wait for decode string']
     * @returns {string}          [return decode string']
     */
    static urlBase64Decode(str) {
        let output = str.replace(/-/g, '+').replace(/_/g, '/');
        switch (output.length % 4) {
            case 0:
                {
                    break;
                }
            case 2:
                {
                    output += '==';
                    break;
                }
            case 3:
                {
                    output += '=';
                    break;
                }
            default:
                {
                    throw 'Illegal base64url string!';
                }
        }
        return window.decodeURIComponent(escape(window.atob(output))); //polyfill https://github.com/davidchambers/Base64.js
    }
    /**
     * @function decodeToken
     * @memberOf app.jwt
     * @description  decodetoken
     * @param  {string}  token       [wait for  decode token]
     * @returns {object}          [return decode token]
     */
    static decodeToken(token) {
        if (typeof token !== 'string') return null;
        if (!token.match(/\./i)) return null;
        let parts = token.split('.');
        if (parts.length !== 3) {
            return null;
        }
        let decoded = JWT.urlBase64Decode(parts[1]);
        if (!decoded) {
            return null;
        }
        return JSON.parse(decoded);
    }
    /**
     * @function getTokenExpirationDate
     * @memberOf app.jwt
     * @description get token expired time
     * @param  {string}  token       [wait for  decode token]
     * @returns {string}          [return expired time]
     */
    static getTokenExpirationDate(token) {
        let decoded = JWT.decodeToken(token);
        if (typeof decoded.exp === "undefined") {
            return null;
        }
        let d = new Date(0); // The 0 here is the key, which sets the date to the epoch
        d.setUTCSeconds(decoded.exp);
        return d;
    }
    /**
     * @function isTokenExpired
     * @memberOf app.jwt
     * @description tell token if expired
     * @param  {string}  token       [wait for  decode token]
     * @param  {number}  offsetSeconds       [offset seconds]
     * @returns {boolean}          [return if expired]
     */
    static isTokenExpired(token, offsetSeconds) {
        let d = JWT.getTokenExpirationDate(token);
        offsetSeconds = offsetSeconds || 0;
        if (d === null) {
            return false;
        }
        // Token expired?
        return !(d.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)));
    }
    /**
     * @function isExpired
     * @memberOf app.jwt
     * @description tell token.exp if expired time
     * @param  {string}  token       [wait for  decode token]
     * @param  {number}  offsetSeconds       [offset seconds]
     * @returns {boolean}          [return if expired]
     */
    static isExpired(exp, offsetSeconds) {
        let d = new Date(0); // The 0 here is the key, which sets the date to the epoch
        d.setUTCSeconds(exp);
        offsetSeconds = offsetSeconds || 0;
        if (d === null) {
            return true;
        }
        // Token expired?
        return !(d.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)));
    }
    /**
     * @function isSoonExpired
     * @memberOf app.jwt
     * @description tell token.exp if expired
     * @param  {string}  token       [wait for  decode token]
     * @param  {number}  offsetSeconds       [offset seconds(alarm before 30 seconds)]
     * @returns {boolean}          [return if expired]
     */
    static isSoonExpired(exp, offsetSeconds) {
        let d = new Date(0); // The 0 here is the key, which sets the date to the epoch
        d.setUTCSeconds(exp);
        offsetSeconds = offsetSeconds || -30;
        if (d === null) {
            return false;
        }
        return (new Date().valueOf() > (d.valueOf() + (offsetSeconds * 1000)));
    }

    //wait for get token
    static getNewToken() {
        return new Promise((resolve, reject) => {
            console.log("getNewToken", JWT.apiUrl() + '/get-token/')
            request.get(JWT.apiUrl() + '/get-token/', {
                timeout: 10000,
                headers: {
                    'Request-New-Token': 'myFirstRRWA'
                },
                responseType: 'json',
            }).then(function(req) {
                if (req && req.data && req.data.token) {
                    let p = localData.setLocalData('jwtToken', req.data.token, true);
                    resolve(req.data.token);
                } else {
                    reject(null);
                }
            }).catch(function(req) {
                reject(null);
            });

        });
    }
    //wait for replace old token to new token
    static refreshToken(postData, token) {
        return new Promise((resolve, reject) => {
            request.post(JWT.apiUrl() + '/refresh-token/', postData, {
                timeout: 10000,
                headers: {
                    'Request-New-Token': 'myFirstRRWA',
                    'Authorization': 'Bearer ' + token
                },
                responseType: 'json',
            }).then(function(req) {
                if (req && req.data && req.data.token) {
                    let p = localData.setLocalData('jwtToken', req.data.token, true);
                    resolve(req.data.token);
                } else {
                    reject(null);
                }
            }).catch(function(req) {
                reject(null);
            });

        });
    }

    static paddingZero(n) {
        if (parseInt(n, 10) < 10) {
            return '0' + n;
        } else {
            return n + '';
        }
    }
    static getDateObj(t, returnString) {
        let dt;
        if (t !== undefined) {
            if (typeof t === 'string' || typeof t === 'number') {
                dt = new Date(t);
            } else {
                dt = new Date();
            }
        } else {
            dt = new Date();
        }
        return returnString ? dt.getFullYear() + "-" + JWT.paddingZero(dt.getMonth() + 1) + "-" + JWT.paddingZero(dt.getDate()) : {
            y: dt.getFullYear(),
            m: JWT.paddingZero(dt.getMonth() + 1),
            d: JWT.paddingZero(dt.getDate()),
            h: JWT.paddingZero(dt.getHours()),
            i: JWT.paddingZero(dt.getMinutes()),
            s: JWT.paddingZero(dt.getSeconds())
        };
    }

    static getLocalTime(){
        let obj = JWT.getDateObj();
        return obj.y+"-"+obj.m+"-"+obj.d+" "+obj.h+":"+obj.i+":"+obj.s;
    }

}

const isLoging = true;

class localData {
    /**
     * @function setLocalData
     * @memberOf pbFunc
     * @description local storage data
     * @param  {string} storageKey [based on local/sessionStoragestorage key]
     * @param  {string} storageValue [based on local/sessionStoragestorage value]
     * @param {boolean} isPermanent [if permanent save]
     * @returns {boolean} [if success]
     */
    static setLocalData(storageKey, storageValue, isPermanent) {
        let _isPermanent = (typeof isPermanent === 'boolean') ? isPermanent : false;
        let windowStorageObj = (_isPermanent) ? window.localStorage : window.sessionStorage;
        //if (pbDebug) console.log('setLocalData', storageKey);
        try {
            windowStorageObj.setItem(storageKey, JSON.stringify(storageValue));
            return true;
        } catch (errStorageObj) {
            console.log('errStorageObj', errStorageObj);
            if (isLoging) throw new Error('Storage data failed! [' + errStorageObj.toString() + ']');
            return false;
        }
        //return false;
    }
    /**
     * @function getLocalData
     * @memberOf pbFunc
     * @description get local storage  data
     * @param  {string} storageKey [based on local/sessionStoragestorage key]
     * @param {boolean} isPermanent [if permanent save]
     * @returns  {string|boolean}  [returnbased on local/sessionStoragestorage value]
     */
    static getLocalData(storageKey, isPermanent) {
        let _isPermanent = (typeof isPermanent === 'boolean') ? isPermanent : false;
        let windowStorageObj = (_isPermanent) ? window.localStorage : window.sessionStorage;
        //if (pbDebug) console.log('getLocalData', storageKey);
        try {
            if (storageKey in windowStorageObj) {
                return JSON.parse(windowStorageObj.getItem(storageKey));
            }
        } catch (errStorageObj) {
            console.log('errStorageObj', errStorageObj);
            if (isLoging) throw new Error('Storage data failed! [' + errStorageObj.toString() + ']');
            return false;
        }
        //return null;
    }
    /**
     * @function delLocalData
     * @memberOf pbFunc
     * @description delete local storage  data
     * @param  {string} storageKey [based on local/sessionStoragestorage key]
     * @param  {function} cb [delete callback]
     * @param {boolean} isPermanent [if permanent save]
     */
    static delLocalData(storageKey, cb, isPermanent) {
        let _isPermanent = (typeof isPermanent === 'boolean') ? isPermanent : false;
        let windowStorageObj = (_isPermanent) ? window.localStorage : window.sessionStorage;
        let storageObj = (_isPermanent) ? localStorage : sessionStorage;
        //if (pbDebug) console.log('delLocalData', storageKey);
        try {
            if (storageKey in windowStorageObj) {
                windowStorageObj.removeItem(storageKey);
                if (typeof cb === 'function') {
                    cb();
                }
            } else if (storageKey in storageObj) {
                storageObj.removeItem(storageKey);
                if (typeof cb === 'function') {
                    cb();
                }
            } else {
                storageObj.removeItem(storageKey);
                if (typeof cb === 'function') {
                    cb();
                }
            }
        } catch (errStorageObj) {
            if (isLoging) throw new Error('Storage data failed! [' + errStorageObj.toString() + ']');
            //return false;
        }
    }
}

export {
    JWT, localData
};