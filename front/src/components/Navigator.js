import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import {Link} from 'react-router'

import styles from '../styles.module.css'
import 'font-awesome/css/font-awesome.css'

//require("url-loader?limit=50000!./bg.jpg&name=[name].[ext]");
//import bg from '../bg.jpg'
//console.log("bg",bg)

class Navigator extends React.Component {

  render() {
    return <div class="fade in  text-center" >
        <h1>
            <Link to="/about"><i className="fa fa-user-secret"></i> <strong>A</strong>bout</Link>
            <Link to="/resume"><i className="fa fa-barcode"></i> <strong>H</strong>istory</Link>
            <a href="http://blog.com/"><i className="fa fa-beer"></i> <strong>B</strong>log</a>
            <Link to="/admin"><i className="fa fa-user-secret"></i> <strong>A</strong>dmin</Link>
        </h1>

        <p className={styles.zdIcon}>
            <a href="javascript:void(0);" className="zdNoborder" rel="tooltip" title="yourname at google.com">
                <span><i className="fa fa-envelope"></i></span>
            </a>
            <a href="javascript:void(0);" className="zdNoborder" rel="tooltip" title="yourname at google.com">
                <span><i className="fa fa-qq"></i></span>
            </a>
            <a href="https://github.com/" className="zdNoborder" rel="tooltip" title="https://github.com/">
                <span><i className="fa fa-github"></i></span>
            </a>
            <a href="javascript:void(0);" className="zdNoborder" rel="tooltip" title="yourname at google.com">
                <span><i className="fa fa-weixin"></i></span>
            </a>
            <a href="https://www.linkedin.com" className="zdNoborder" rel="tooltip" title="linkedin">
                <span><i className="fa fa-linkedin"></i></span>
            </a>
            <a href="https://twitter/" className="zdNoborder" rel="tooltip" title="twitter">
                <span><i className="fa fa-twitter"></i></span>
            </a>
        </p>
      </div>;

  }
}

module.exports = Navigator;