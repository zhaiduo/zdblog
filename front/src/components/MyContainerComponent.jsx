import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import store from '../store.jsx'
import { todoApp } from '../action.jsx'
import request from 'axios';

import { JWT, localData } from './JWT'
import BlogItemComponent from './BlogItemComponent.jsx'

import styles from '../styles.module.css'
import 'font-awesome/css/font-awesome.css'

//const API_URL = 'http://localhost:8088'; //'http://zhaiduo.com:8080';//
//const API_URL = 'http://localhost:8003'; //'http://zhaiduo.com:8080';//
//http://localhost:8080/post/1

const getLastId = () => {
  return new Promise((resolve, reject) => {
    request
      //.get(JWT.apiUrl() + '/post/?_sort=id&_order=ASC&_limit=1')
      .get(JWT.apiUrl() + '/post/?_sort=wp:post_date&_order=DESC&_limit=1')
      .then(function(req) {
        console.log('getLastId', req.data[0]);
        resolve(req.data[0].id)
      })
      .catch(function(req) {
        console.log('getLastId err', req);
        reject(null)
      });
  });

}


// Create the component that you want to make smart.
class MyContainerComponent extends React.Component {

  getPost(id) {
    let self = this;
    return (dispatch) => {
      //console.log('getPost', dispatch, id, self);
      if (id) {
        request
          .get(JWT.apiUrl() + '/post/' + id)
          .then(function(req) {
            //console.log('req', req);
            store.dispatch(self.setPost(req.data));
            setTimeout(function() {
              self.setState({}); // Just trigger a render
            }, 100);
          })
          .catch(function(req) {
            store.dispatch(self.setPostError(id));
          });
      } else {
        store.dispatch(self.setPostError(id));
      }
    };
  }
  setPost(data) {
    //console.log('GET_POST', data);
    return {
      type: 'GET_POST',
      data
    };
  }
  setPostError(id) {
    return {
      type: 'GET_POST_ERROR',
      id
    };
  }

  getNextPostId(data) {
    if (!data || !data.id) return 1;
    let curId = parseInt(data.id, 10)
    return curId >= 596 ? curId - 596 : curId + 1;
  }

  getNextPost() {
    let self = this;
    if (this.props.data && this.props.data.id) {
      let id = this.getNextPostId(this.props.data);
      console.log('getNextPost', this.props.data.id, id);
      return (dispatch) => {
        //console.log('getNextPost', dispatch, id, self);
        if (id) {
          request
            .get(JWT.apiUrl() + '/post/' + id)
            .then(function(req) {
              //console.log('req', req);
              store.dispatch(self.nextPost(req.data));
              self.getPosts(10, id);
              setTimeout(function() {
                self.setState({}); // Just trigger a render
              }, 100);
            })
            .catch(function(req) {
              store.dispatch(self.nextPostError(id));
            });
        } else {
          store.dispatch(self.nextPostError(id));
        }
      };
    } else {

    }
  }
  nextPost(data) {
    //console.log('NEXT_POST', data);
    return {
      type: 'NEXT_POST',
      data
    };
  }
  nextPostError(id) {
    return {
      type: 'NEXT_POST_ERROR',
      id
    };
  }

  getPosts(offset, start) {
    let self = this;
    if (offset) {

      let _start = (typeof start === 'string' && parseInt(start, 10) < 1000) ? parseInt(start, 10) : 1;
      let _offset = (typeof offset === 'string' && parseInt(offset, 10) < 20) ? parseInt(offset, 10) : (start > 597) ? start - 597 : 10;
      //console.log('getPosts', offset, start)
      console.log('getPosts==', _start, _offset, this.props.data.id)
      const ordby = (_start != 1) ? 'ASC' : 'DESC';
      //console.log('getPosts', typeof start, offset, start, '/post/?_start='+_start+'&_limit='+_offset+'&_sort=id&_order=ASC');
      //_sort=id&_order=DESC&_limit=1
      request
        //.get(JWT.apiUrl() + '/post/?_start='+_start+'&_limit='+_offset+'&_sort=id&_order=ASC')
        .get(JWT.apiUrl() + '/post/?_start=' + _start + '&_limit=' + _offset + '&_sort=wp:post_date&_order=' + ordby)
        .then(function(req) {
          //console.log('req', self.props.data, req.data);
          let newData = self.props.data;
          newData.recentPosts = req.data;
          //console.log("newData.recentPosts", newData.recentPosts);
          store.dispatch(self.setPosts(newData));
          setTimeout(function() {
            self.setState({}); // Just trigger a render
          }, 100);

        })
        .catch(function(req) {
          store.dispatch(self.setPostsError(offset));
        });
    } else {
      store.dispatch(self.setPostsError(offset));
    }

  }
  setPosts(data) {
    //console.log('GET_POSTS', data);
    return {
      type: 'GET_POSTS',
      data
    };
  }
  setPostsError(offset) {
    return {
      type: 'GET_POSTS_ERROR',
      offset
    };
  }

  constructor(props) {
    super(props);
  //console.log('props', props, this);
  /*this.state = {
  };*/
  }

  componentWillMount() {
    let self = this;
    let trgId = 1;

    getLastId().then(n => {
      trgId = parseInt(n, 10);
      //console.log('trgId', trgId);
      if (this.props.pid && this.props.pid.match(/^[0-9]+$/i))
        trgId = this.props.pid;
      request
        .get(JWT.apiUrl() + '/post/' + trgId)
        .then(function(req) {
          store.dispatch(self.setPost(req.data));
          self.getPosts(10, trgId);
          setTimeout(function() {
            self.setState({}); // Just trigger a render
          }, 100);
        })
        .catch(function(req) {
          store.dispatch(self.setPostError(trgId));
        });
    });

  }
  componentDidMount() {
    //this.getPosts(10, trgId);
  }

  tidyData(data) {
    //console.log('tidy', data);
    if (typeof data !== 'string') {
      if (typeof data === 'object' && data.hasOwnProperty('$t')) {
        var tmp = data.$t;
        return tmp.replace(/[\n]/ig, "<br>");
      }
    }
    return data;
  }

  tidyContent(data) {
    //console.log('tidyContent', data);
    if (typeof data === 'object' && data.hasOwnProperty('content:encoded')) {
      return this.tidyData(data['content:encoded']);
    }
    return data;
  }

  render() {
    //console.log('render', this.props.data);
    //let data = store.getState();
    let title,
      nextId,
      prevId,
      nextLink,
      prevLink;
      //console.log('store.getState', this.props);

    // And you have access to the selected fields of the State too!
    if (this.props.data) {
      //console.log('this.props.data.recentPosts', this.props.data.recentPosts);
      //Environment: {__NODE_ENV__}
      if (this.props.pid !== undefined) {
        title = <a href={ "/" + this.props.pid }>
                  { this.props.data.title ? this.tidyData(this.props.data.title) : 'No Title' }
                </a>;
      } else {
        title = this.props.data.title ? this.tidyData(this.props.data.title) : 'No Title';
      }

      nextId = this.getNextPostId(this.props.data)
      nextLink = <link
                         rel="next"
                         id={ nextId } />
      prevId = this.props.data.id
      if (prevId)
        prevLink = <link
                         rel="prev"
                         id={ prevId } />

      //console.log("nextId", nextId)
      //console.log("prevId", prevId)

      if (this.props.data.recentPosts) {
        return <div className={ styles.container }>
                 <a name="title"></a>
                 { nextLink }
                 { prevLink }
                 <h1 className={ styles.h1 }><i className="fa fa-star"></i> { title }</h1>
                 <BlogItemComponent blogData={ this.props.data }>
                 </BlogItemComponent>
                 <p className={ styles.center }>
                   <br/>
                   <br/>
                   <button onClick={ this.getNextPost() }>
                     Next Page
                   </button>
                 </p>
                 <hr />
                 <div className={ styles.center }>
                   <h3>Recent Articles</h3>
                 </div>
                 <div className={ styles.center }>
                   <ol start={ this.props.data.id + 1 }>
                     { this.props.data.recentPosts.map((post, index) => {
                         //console.log('map', post, index);
                         if (index => 0) return <li
                                                    className={ styles.left }
                                                    key={ index }>
                                                  <a href={ '/' + post.id }>
                                                    { post.title ? this.tidyData(post.title) : 'No Title' }
                                                  </a> - <span className={ styles.small }>{ post.pubDate ? this.tidyData(post.pubDate) : '' }</span>
                                                </li>
                       }) }
                   </ol>
                 </div>
               </div>;
      } else {
        return <div className={ styles.container }>
                 <h1 className={ styles.h1 }><i className="fa fa-star"></i> { title }</h1>
                 <BlogItemComponent blogData={ this.props.data }>
                 </BlogItemComponent>
                 <p className={ styles.center }>
                   <br/>
                   <br/>
                   <button onClick={ this.getNextPost() }>
                     Next Page
                   </button>
                 </p>
               </div>;
      }
    } else {
      return <div className={ styles.center }>
               [404] Nothing Found
             </div>;
    }

  }
}

/*MyContainerComponent.propTypes = {
  onClick: PropTypes.func.isRequired
};
MyContainerComponent.defaultProps = {

};
*/


// Select the props which you want to inject in the component,
// given the global state.
function select(state) {
  //console.log('select', state, state.blog.data);
  return {
    data: state.blog.data
  };
}

// Wrap the component to inject the Dispatcher and the State into it.
export default connect(select)(MyContainerComponent);

