import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import {Link} from 'react-router'

import styles from '../styles.module.css'
import 'font-awesome/css/font-awesome.css'

import Navigator from './Navigator'

class About extends React.Component {
  render() {
    return (
      <div>
      <Navigator></Navigator>
      <div className="fade in  text-center"  >

        <p>
            <strong>About</strong>
        </p>

        <div className="fade in  text-center main">

            <p>Blah, Blah, Blah, Blah</p>

            <p className="zdMgTp40"><Link to="/"><span>Home</span></Link></p>

        </div>
      </div>
      </div>
    )
  }
}

module.exports = About;