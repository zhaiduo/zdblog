import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'
import {Link} from 'react-router'

import styles from '../styles.module.css'
import 'font-awesome/css/font-awesome.css'

import Navigator from './Navigator'

class Resume extends React.Component {
  render() {
    return (
      <div>
      <Navigator></Navigator>
      <div className="fade in  text-center"  >

        <p>
            <strong>History</strong>
        </p>

        <div className="fade in  text-center main">

            <div className="row">
                <div className="col-sm-3 col-md-3 col-lg-3 text-right zdNoPdLR">Status </div>
                <div className="col-sm-9 col-md-9 col-lg-9 text-left zdNoPdLR zdPdLf10"><strong>Working as Frontend Developer</strong> </div>
            </div>


            <p className="zdMgTp40"><Link to="/"><span>Home</span></Link></p>

        </div>
      </div>
      </div>
    )
  }
}

module.exports = Resume;