import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'

import styles from '../styles.module.css'
//import 'css/font-awesome.css'
//import 'css/font/fontawesome-webfont.woff2?v=4.3.0'

class Editor extends React.Component {
    constructor(props) {
        super(props);
        this.iconAction = this.iconAction.bind(this);
        let iconArr = []
        const iconObj = {
            bold: 'Bold',
            align_left: 'Align Left',
            align_center: 'Align Center',
            align_right: 'Align Right',
            list: 'List',
            picture_o: 'Image',
            //cloud_upload: 'Upload',
            paperclip: 'Link',
            mail_reply: 'Cancel',
            mail_forward: 'Redo',
            remove: 'Clear',
            code: 'HTML',
            //https://www.w3schools.com/icons/fontawesome_icons_webapp.asp
        }
        for(let t in iconObj){
            iconArr.push({
                name: t.replace(/_/i, '-'),
                title: iconObj[t],
                class: 'fa fa-'+t.replace(/_/i, '-'),
            });
        }
        this.state.icons = iconArr;

    }
    state = {
        id: this.props.id,
        className: this.props.className,
    }
    componentDidMount() {
       this.elm = document.querySelector('#u_editor');
       this.elm_edit = document.querySelector('#u_editor_edit');
       this.elm.addEventListener('mouseup', this.editorEvnt.bind(this), false);
    }
    editorEvnt = e => {
        //console.log("editorEvnt", e)
        e.stopPropagation();

        //get mouse selection
        let selection = null;
        let start = 0;
        let end = 0;
        let base = 0;
        let extent = 0;
        let range, selectedTextNode, selected = false,
            selectedFocusNode, selectedBaseNode, selectedExtentNode, tempParentNode, previousNode, isFoundTag;
        if (true) {
            if (window.getSelection) {
                selection = window.getSelection();
            } else if (document.selection) {
                selection = document.selection.createRange();
            }

            if (selection) {
                start = selection.anchorOffset || 0;
                end = selection.focusOffset || 0;
                base = selection.baseOffset || 0;
                extent = selection.extentdOffset || 0;
                selectedTextNode = selection.anchorNode ? selection.anchorNode.data : "";
                selectedFocusNode = selection.focusNode ? selection.focusNode.data : "";
                selectedBaseNode = selection.baseNode ? selection.baseNode.data : "";
                selectedExtentNode = selection.extentNode ? selection.extentNode.data : "";
            }
            //IE no selection.rangeCount
            if (selection && selection.getRangeAt) {
                range = selection.getRangeAt(0);
                if (selection.isCollapsed === false && range.collapsed === false) {
                    if (this.selectedRangeObj === null) this.selectedRangeObj = {
                        //endContainer: range.endContainer,
                        endOffset: range.endOffset,
                        //startContainer: range.startContainer,
                        startOffset: range.startOffset
                    };
                }
                console.log("getRangeAt", range)
            }
            if (selection && range) {
                selection.removeAllRanges();
                selection.addRange(range);
                //range.collapse(false);
            }

            //IE no type: selection.type && selection.type.match(/^range$/i) &&
            if (selection.isCollapsed === false) {
                //console.log("range", selection.toString(), start, end, selection)

                    this.selectedText = selection.toString();
                    this.selectedTextOffsetStart = start;
                    this.selectedTextOffsetEnd = end;
                    this.selectedTextOffsetBase = base;
                    this.selectedTextOffsetExtent = extent;
                    this.selectedTextNode = selectedTextNode;
                    this.selectedFocusNode = selectedFocusNode;
                    this.selectedBaseNode = selectedBaseNode;
                    this.selectedExtentNode = selectedExtentNode;
                    this.isSelected = true;
                    this.range = range;

            } else if (selection.isCollapsed === true) {

                //console.log("caret", selection.toString(), start, end, selection)
                this.selectedText = selection.toString();
                this.selectedTextOffsetStart = start;
                this.selectedTextOffsetEnd = end;
                this.selectedTextOffsetBase = base;
                this.selectedTextOffsetExtent = extent;
                this.selectedTextNode = selectedTextNode;
                this.selectedFocusNode = selectedFocusNode;
                this.selectedBaseNode = selectedBaseNode;
                this.selectedExtentNode = selectedExtentNode;
                this.isSelected = false;
                this.range = range;

            } else {
                //console.log("selection", selection.toString(), start, end, selection)
            }
        }

    };

    iconElm = null
    elm = null
    //isSelected = false
    hasError = false

    tempHtml = '';
    tempText = '';

    selectedTextNode = '';
    selectedFocusNode = '';
    selectedBaseNode = '';
    selectedExtentNode = '';
    range = null;
    selectedText = '';
    isSelected = false;
    selectedTextOffsetStart = 0;
    selectedTextOffsetEnd = 0;
    selectedTextOffsetBase = 0;
    selectedTextOffsetExtent = 0;

    htmlCacheList = [];
    htmlCacheListPointer = 0;
    syncCacheList = function() {
        this.htmlCacheListPointer = this.htmlCacheList.length - 1;
        if (this.htmlCacheList.length > 20) {
            this.htmlCacheList.pop();
            this.htmlCacheListPointer = this.htmlCacheList.length - 1;
        }
    };
    saveCacheList = function(html) {
        this.htmlCacheList.unshift(html);
        this.syncCacheList();
    };

    stripTags = html => {
        return html.replace(/<[^<>]+>/gi, "");
    }
    escapeTags = html => {
        return html.replace(/</gi, "&lt;").replace(/>/gi, "&gt;");
    }
    unescapeTags = html => {
        return html.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
    }
    inArray = (needle, array) => {
        for (let i = 0, imax = array.length; i < imax; i++) {
            if (needle === array[i]) {
                return true;
            }
        }
        return false;
    }
    safeSaveHtml = (data, cbOk, isSave) => {
        let maxEditLength = 102400;
        let maxEditLengthTitle = '100KB';
        let _isSave = typeof isSave === 'boolean' ? isSave : true;
        //console.log("len ", data.length)
        if (data.length > maxEditLength) {
            //$scope.showMenu = false;
            this.hasError = true;
            alert("Content length is longer than " + maxEditLengthTitle + ".");
            return false;
        } else {
            if (_isSave) {
                this.elm.innerHTML=data;
                //console.log("$scope.elm", $scope.elm.attr('data-editor'))
                /*this.$emit('editor:update', {
                    target: $scope.elm.attr('data-editor'),
                    html: data
                });*/
            }
            this.hasError = false;
            if (typeof cbOk === 'function') {
                cbOk();
            }
        }
    }
    cnStrlen = (str) => {
        let i = 0;
        let c = 0;
        let unicode = 0;
        let len = 0;
        if (str != undefined && typeof str == "string") {
            str = str.trim();
            if (str == "") {
                return 0;
            } else {
                len = str.length;
                for (i = 0; i < len; i++) {
                    unicode = str.charCodeAt(i);
                    if (unicode < 127) { //判断是单字符还是双字符
                        c += 1;
                    } else { //chinese
                        c += 2;
                    }
                }
                return c;
            }
        } else {
            return 0;
        }
    }
    isCanAddTag = () => {
        return this.cnStrlen(this.selectedText) / 2 === Math.abs(this.selectedTextOffsetEnd - this.selectedTextOffsetStart) ? true : false;
    };

    isTagStyle = (tempParentNode) => {
        return (tempParentNode && tempParentNode.tagName === 'SPAN' && tempParentNode.className === "u-tmpl-label") ? true : false
    };

    addTag = (currentString, tag, style) => {
        //console.log("addTag", currentString, this.selectedTextOffsetStart, this.selectedTextOffsetEnd)
        let _style = style ? ' ' + style : '';
        let start = parseInt(this.selectedTextOffsetStart, 10);
        let end = parseInt(this.selectedTextOffsetEnd, 10);
        if (start > end) end = start + end;
        let startStr = currentString.substring(0, start);
        let focusStr = currentString.substring(start, end);
        let endStr = currentString.substring(end, currentString.length);
        let newString = startStr + '<' + tag + '' + _style + '>' + focusStr + '</' + tag + '>' + endStr;
        return newString;
    };

    getTag = (currentString, tag, style) => {
        let _style = style ? ' ' + style : '';
        return '<' + tag + '' + _style + '>' + currentString + '</' + tag + '>';
    };

    findPElem = (parentElem) => {
        let isParentP = true;
        let tryI = 0;
        while (parentElem.tagName !== 'P' && tryI < 10) {
            isParentP = false;
            tryI++;
            if (parentElem.parentNode) {
                parentElem = parentElem.parentNode;
                if (parentElem.tagName === 'P') {
                    isParentP = true;
                    break;
                }
            }
        }
        return isParentP;
    };

    getAroundHtml = (container, siblingName) => {
        //previousSibling nextSibling
        let currentSibling;
        if (container[siblingName]) {
            currentSibling = container[siblingName];
        } else {
            currentSibling = container.parentNode[siblingName];
        }
        //range.endContainer.parentNode.nextSibling

        let beforeHtml = [];
        while (currentSibling) {
            if (currentSibling.nodeName === '#text') {
                if (siblingName === 'previousSibling') {
                    beforeHtml.unshift(currentSibling.nodeValue);
                } else {
                    beforeHtml.push(currentSibling.nodeValue);
                }
            } else {
                if (siblingName === 'previousSibling') {
                    //console.log("currentSibling.outerHTML", currentSibling.outerHTML)
                    if (!currentSibling.outerHTML.match(/data\-name=\"editor\-menu\"/i)) {
                        beforeHtml.unshift(currentSibling.outerHTML);
                    }
                } else {
                    beforeHtml.push(currentSibling.outerHTML);
                }
            }
            if (currentSibling[siblingName]) {
                currentSibling = currentSibling[siblingName]
            } else {
                break;
            }
        }
        return beforeHtml.join("");
    };

    customizeStyle = (tag, style) => {
        let _style = style ? style : '';
        if (tag === "a") {
            let retVal = prompt("Please enter the URL: ", "http://");
            if (retVal) {
                retVal = retVal.replace(/[^0-9a-z_\.\/\?#&=%:;\+\-]+$/gi, "");
                if (retVal.match(/^https?:\/\/[0-9a-z_\.\/\?#&=%:;\+\-]+$/i)) {
                    _style = 'url="' + retVal + '"';
                }
            }
        }
        return _style;
    };
    handlText = (tag, style) => {
        console.log("handlText", tag, style, this.isSelected)
        let valid_tags = ['strong', 'i', 'a', 'span'];
        if (this.isSelected && !this.inArray(tag, valid_tags)) return false;
        let html = this.elm.innerHTML;
        let parentElem;
        this.safeSaveHtml(html, function() {}, false);
        let currentString = (this.selectedTextNode) ? this.selectedTextNode : '';
        if (html && this.isSelected === true) {
            let beforeHtml = this.getAroundHtml(this.range.startContainer, 'previousSibling');
            let afterHtml = this.getAroundHtml(this.range.endContainer, 'nextSibling');
            //console.log("beforeHtml", beforeHtml)
            //console.log("afterHtml", afterHtml)
            let currentHtml = '';
            let tmpHtml = '';
            //console.log("this.range", this.range)
            if (this.range.startContainer.data === this.range.endContainer.data) {
                if (this.range.startContainer.previousSibling === null && this.range.endContainer.nextSibling === null) {
                    currentHtml = this.range.startContainer.data;
                    if (!currentHtml.match(/[<]/i)) {
                        style = this.customizeStyle(tag, style);
                        currentHtml = this.addTag(this.range.startContainer.data, tag, style);
                    }
                } else {
                    style = this.customizeStyle(tag, style);
                    currentHtml = this.addTag(this.range.startContainer.data, tag, style);
                }
            } else {
                if (this.range.startContainer.previousSibling === null && this.range.endContainer.nextSibling) {
                    style = this.customizeStyle(tag, style);
                    tmpHtml = this.addTag(this.range.endContainer.data, tag, style);
                    //console.log("tmpHtml", tmpHtml)
                    currentHtml = this.getTag(this.range.startContainer.data, tag, style) + tmpHtml;
                } else if (this.range.startContainer.previousSibling && this.range.endContainer.nextSibling === null) {
                    style = this.customizeStyle(tag, style);
                    tmpHtml = this.addTag(this.range.startContainer.data, tag, style);
                    //console.log("tmpHtml", tmpHtml)
                    currentHtml = tmpHtml + this.getTag(this.range.endContainer.data, tag, style);
                } else if (this.range.startContainer.previousSibling === null && this.range.endContainer.nextSibling === null) {
                    currentHtml = this.range.startContainer.data + this.range.endContainer.data;
                } else {

                }
            }

            if (currentHtml !== "") this.safeSaveHtml("<p>" + beforeHtml + currentHtml + afterHtml + "</p>", function() {});

        } else {
            if (tag.match(/^(align\-|ul)/i)) {
                //pbFunc.objPrint(this.range);
                if (this.range && this.range.endContainer) {
                    //pbFunc.objPrint(this.range.endContainer, 3, 0, 0);
                    if (this.range.endContainer.parentElement) {
                        parentElem = this.range.endContainer.parentElement;
                    } else {
                        if (this.range.endContainer.parentNode) {
                            parentElem = this.range.endContainer.parentNode;
                        } else {
                            parentElem = this.range.endContainer;
                        }
                    }
                    //console.log("parentElem", parentElem)
                    //pbFunc.objPrint(parentElem, 3, 0, 0);
                    let isParentP = this.findPElem(parentElem);
                    //console.log("parentElem", parentElem)
                    if (isParentP) {
                        if (tag.match(/^ul$/i)) {
                            parentElem.innerHTML = '<ul class="u-editor-ul"><li>' + parentElem.innerHTML + '</li></ul>';
                            this.safeSaveHtml(this.elm.innerHTML, function() {}, false);
                        } else {
                            parentElem.setAttribute('class', 'f-text-' + tag);
                        }
                    }else{
                        parentElem = document.querySelector('#u_editor');
                        if(parentElem.childNodes[0].tagName === 'P'){
                            parentElem.childNodes[0].removeAttribute('class');
                            if (tag.match(/^ul$/i)) {
                                parentElem.childNodes[0].remove();
                                parentElem.innerHTML = '<ul class="u-editor-ul"><li>' + parentElem.innerHTML + '</li></ul>';
                                this.safeSaveHtml(this.elm.innerHTML, function() {}, false);
                            } else {
                                parentElem.childNodes[0].innerHTML = parentElem.innerHTML;
                                //console.log("parentElem.innerHTML", parentElem.innerHTML, parentElem)
                                parentElem.childNodes[0].setAttribute('class', 'f-text-' + tag);
                            }
                        }else{
                            if (tag.match(/^ul$/i)) {
                                parentElem.innerHTML = '<ul class="u-editor-ul"><li>' + parentElem.innerHTML + '</li></ul>';
                                this.safeSaveHtml(this.elm.innerHTML, function() {}, false);
                            } else {
                                parentElem.innerHTML = '<p>' + parentElem.innerHTML + '</p>';
                                console.log("parentElem.innerHTML", parentElem.innerHTML, parentElem)
                                parentElem.childNodes[0].setAttribute('class', 'f-text-' + tag);
                            }
                        }
                    }
                }
            } else if (tag.match(/^uls$/i)) {
                if (this.range.endContainer) {
                    parentElem = (this.range.endContainer.parentNode) ? this.range.endContainer.parentNode : this.range.endContainer.parentElement;
                    //pbFunc.objPrint(parentElem, 3, 0, 0);
                    if (parentElem.nodeName === 'P' && !parentElem.innerHTML.match(/[<>]/i)) parentElem.innerHTML = '<ul class="u-editor-ul"><li>' + this.range.endContainer.data + '</li></ul>';
                }
            } else if (tag.match(/^img$/i)) {
                if (tag.match(/^img$/i)) {
                    let retVal = prompt("Please enter Image URL: ", "http://");
                    if (retVal) {
                        retVal = retVal.replace(/[^0-9a-z_\.\/\?#&=%:;\+\-]+$/gi, "");
                        if (retVal.match(/^https?:\/\/[0-9a-z_\.\/\?#&=%:;\+\-]+\.(gif|jpg|webp|jpeg|png)$/i)) {
                            let newImg = document.createElement("img");
                            newImg.setAttribute('class', 'u-editor-img');
                            newImg.setAttribute('src', retVal);
                            newImg.setAttribute('border', '0');
                            this.range.insertNode(newImg);
                        }
                    }
                }
            } else if (tag.match(/^a$/i)) {
                let newA = document.createElement("span");
                newA.setAttribute('class', 'u-tmpl-label');
                //newA.innerHTML = pbFunc.tmplVarDelTag + style;
                newA.innerHTML = style;
                //console.log('newA', newA);
                if (this.range) {
                    this.range.insertNode(newA);
                    this.safeSaveHtml(this.elm.innerHTML, function() {});
                }
            }
        }

        let _selection, allTagElems, currentTagElem;
        if (window.getSelection) {
            _selection = window.getSelection();
        } else if (document.selection) {
            _selection = document.selection.createRange();
        }
        //pbFunc.objPrint(this.range, 3, 0, 0);
        if (_selection && this.range) {

            if (this.isSelected === true) {
                if (this.range.startContainer.querySelectorAll) {
                    //pbFunc.objPrint(this.range.startContainer, 3, 0, 0);
                    allTagElems = this.range.startContainer.querySelectorAll(tag);
                    //console.log("", )
                    for (let j in allTagElems) {
                        //pbFunc.objPrint(allTagElems[j], 3, 0, 0);
                        if (allTagElems[j].innerHTML === this.selectedText) {
                            currentTagElem = allTagElems[j];
                            break;
                        }
                    }
                }
                //pbFunc.objPrint(currentTagElem, 3, 0, 0);
                if (currentTagElem) {
                    this.range.selectNodeContents(currentTagElem);
                    this.range.collapse(false);
                }
            }
            _selection.removeAllRanges();
            _selection.addRange(this.range);
            //this.range.collapse(false);

        }

        this.saveCacheList(this.elm.innerHTML);
    }
    iconActions = {
        code: e => {
            //const isSource = this.elm.getAttribute("contenteditable") == "true" ? false : true;
            const isSource = this.elm_edit.style.display == 'none' ? false : true;
            console.log('html', isSource)
            const dataEdit = this.elm_edit.value;
            if(isSource){
                this.elm_edit.style.display = 'none';
                //this.elm.innerHTML = '' + this.unescapeTags(this.elm.innerHTML) + '';
                //this.elm.setAttribute("contenteditable", true);
                //console.log('html2', this.elm.getAttribute("contenteditable"), this.elm.innerHTML)
                this.elm.innerHTML = dataEdit;
            } else {
                this.elm_edit.style.display = 'block';
                this.elm_edit.value = this.elm.innerHTML;
                //this.elm.setAttribute("contenteditable", false);
                //this.elm.innerHTML = '' + this.escapeTags(this.elm.innerHTML) + '';
                //console.log('html3', this.elm.getAttribute("contenteditable"), this.elm.innerHTML)
                // setTimeout(()=>{
                //     this.elm.setAttribute("contenteditable", true);
                // }, 400)
                
            }
            //.setAttribute("contenteditable", false);
            //contentEditable
            //this.elm.innerHTML = '' + this.escapeTags(this.elm.innerHTML) + '';
        },
        bold: e => {
            console.log('bold', e)
            this.handlText('strong', '')
        },
        align_left: e => {
            this.handlText('align-left')
        },
        align_center: e => {
            this.handlText('align-center')
        },
        align_right: e => {
            this.handlText('align-right')
        },
        list: e => {
            this.handlText('ul')
        },
        picture_o: e => {
            this.handlText('img')
        },
        cloud_upload: e => {

        },
        paperclip: e => {
            this.handlText('a')
        },
        mail_reply: e => {

        },
        mail_forward: e => {

        },
        remove: e => {
            this.elm.innerHTML = '<p>' + this.stripTags(this.elm.innerHTML) + '</p>';
        },
    }
    iconAction(e) {
        //e.stopPropogation();
        this.iconElm = e.target;
        let className = e.target.getAttribute('class');
        if(className.match(/fa\-([0-9a-z\-]+)/i)){
            className = RegExp.$1.replace(/\-/i,'_');
            if(this.iconActions.hasOwnProperty(className)){
                //console.log('iconAction', className, e)
                this.iconActions[className](e);
            }
        }
    }
    render() {
        let icons = [];
        for(let t in this.state.icons){
            icons.push(<li key={this.state.icons[t].name} class={this.state.icons[t].name}><i title={this.state.icons[t].title} className={this.state.icons[t].class} onClick={this.iconAction}></i></li>);
        }
        return (<div className={styles.editorBox} >
            <ul data-name="editor-menu" id={this.state.id+'-menu'} className={styles.editorMenu}>
                {icons}
            </ul>
            <div id="u_editor" contentEditable="true" className={styles.editor}></div>
            <textarea style={{
                display:'none',
            }} id="u_editor_edit" className={styles.editor2}></textarea>
          </div>);

    }
}

module.exports = Editor;