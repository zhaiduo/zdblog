export function blogApp(state = {
    data: "empty"
}, action) {
    //console.log('action', action);
    switch (action.type) {
        case 'SOME_ACTION':
            return {
                    ...state,
                data: action.data
            }
        case 'UPDATE':
            return {
                    ...state,
                data: action.data
            }
        case 'GET_POST':
            return {
                    ...state,
                data: action.data
            }
        case 'GET_POST_ERROR':
            return {
                    ...state,
                data: action.data
            }
        case 'NEXT_POST':
            return {
                    ...state,
                data: action.data
            }
        case 'NEXT_POST_ERROR':
            return {
                    ...state,
                data: action.data
            }
        case 'GET_POSTS':
            return {
                    ...state,
                data: action.data
            }
        case 'GET_POSTS_ERROR':
            return {
                    ...state,
                data: action.data
            }
        default:
            return state
    }
}
