import React from 'react'
import ReactDOM from 'react-dom'
/*import { combineReducers, applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import MyContainerComponent from './components/MyContainerComponent.jsx'
import store from './store.jsx'
import {todoApp} from './action.jsx'*/

import {browserHistory, Router, Route, Redirect, Link} from 'react-router'
import App from './containers/App/App'
import Home from './components/Home'
import Post from './components/Post'
import About from './components/About'
import Admin from './components/Admin'
import Resume from './components/Resume'

//import makeRoutes from './routes'

export const makeRoutes = () => (
    <Router>
      <Route path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/admin" component={Admin} />
      <Route path="/resume" component={Resume} />
      <Route path="/:pid" component={Post} />
      <Redirect from="*" to="/" />
    </Router>
  )

const routes = makeRoutes()

// Render the Provider component with the the Store
// in the props at the root of the hierarhy.
/*ReactDOM.render(
  <Provider store={store}>
    <MyContainerComponent />
  </Provider>,
  document.getElementById('container') //container
)*/

const mountNode = document.querySelector('#app');
ReactDOM.render(<App history={browserHistory} routes={routes}></App>, mountNode);