import { combineReducers, applyMiddleware, createStore, compose } from 'redux'
import {blogApp} from './action.jsx'


function getStore() {
    const reducer = combineReducers({
        blog: blogApp
    })
    // Apply this middleware to the Store.
    return applyMiddleware()(createStore)(reducer, window.devToolsExtension && window.devToolsExtension());
}
const store = getStore();

export default store;