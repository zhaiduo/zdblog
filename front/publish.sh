# publish www to username@IP/Data/to/domain.com/
# https://youdomain.com/ => /Data/to/domain.com/www/
USER=username
HOST=IP
DIR=/Data/to/domain.com/
rsync -avz --rsh='ssh -p22' www/ ${USER}@${HOST}:${DIR}
echo "Done: https://youdomain.com/"
