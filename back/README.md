# Zdblog Backend
This backend is based on json-server, because it is the fusion of the best API and simplest database for backend development.

## How to start backend?
```
nvm use 6.9.1
cd back
npm install --python=/Users/youinstalled/python2
cp .env.example .env (edit admin name and password)
npm run server
```
then open http://localhost:8003 with your browser.