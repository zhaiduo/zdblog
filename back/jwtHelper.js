// server.js by zhaiduo@gmail.com @2016
const jwt = require('jsonwebtoken');
require('dotenv').config();

export const secret = process.env.ADM_SECRET;
export const createToken = (user = 'zhaiduo.com') => {
  let token = jwt.sign({
    auth: user,
    iat: Math.floor(Date.now() / 1000) - 60
  }, secret, {
    expiresIn: 94605000
  });
  return token;
//backdate a jwt 30 seconds
//return jwt.sign(_.omit(user, 'password'), secret, { expiresIn: 60*60*5 });
}

