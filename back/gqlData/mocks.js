// server.js by zhaiduo@gmail.com @2016
import faker from 'faker';

export const Mocks = {
  Date: () => new Date(),
  Int: () => parseInt(Math.random() * 100, 10),
  String: () => 'It works!',
  Query: () => ({
    blogPost: (root, args) => ({
      id: args.id,
      contentEncoded: {
        pid: args.id
      }
    }),
  }),
  ContentEncoded: () => ({
    _t: faker.lorem.sentences(Math.random() * 3),
  }),
  Link: () => ({
    _t: 'http://'+faker.lorem.words(Math.random() * 1),
  }),
};
export default Mocks;
