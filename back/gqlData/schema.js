// server.js by zhaiduo@gmail.com @2016

export const Schema = [
  `
  scalar Date
  type Title {
    _t: String
  }
  type ContentEncoded {
    _t: String
  }
  type PubDate {
    _t: Date
  }
  type Link {
    _t: String
  }
  # blog post
  type BlogPost {
    id: Int! # unique id
    title: Title #post title
    contentEncoded: ContentEncoded #post content
    link: Link # link
    pubDate: PubDate # date
  }
  type Auth {
    id: Int! # unique id
    user: String!
    passwd: String!
    jwt: String
  }
  type Query {
    blogPost(id: Int!): BlogPost
    blogPosts(first: Int, offset: Int): [BlogPost]
  }

  type Mutation {
    createBlogpost(title: String!, contentEncoded: String!): BlogPost
    signInAuth(account: String!, password: String!): Auth
  }

  schema {
    query: Query
    mutation: Mutation
  }`,
];
export default Schema;



export const Schema = [
  `
  scalar Date
  type Title {
    _t: String
  }
  type ContentEncoded {
    _t: String
  }
  type PubDate {
    _t: Date
  }
  type Link {
    _t: String
  }
  # blog post
  type BlogPost {
    id: Int! # unique id
    title: Title #post title
    contentEncoded: ContentEncoded #post content
    link: Link # link
    pubDate: PubDate # date
  }
  type Auth {
    id: Int! # unique id
    user: String!
    passwd: String!
    jwt: String
  }
  type Query {
    blogPost(id: Int!): BlogPost
    blogPosts(first: Int, offset: Int): [BlogPost]
  }

  type Mutation {
    createBlogpost(title: String!, contentEncoded: String!): BlogPost
    signInAuth(account: String!, password: String!): Auth
  }

  schema {
    query: Query
    mutation: Mutation
  }`,
];
export default Schema;

