// server.js by zhaiduo@gmail.com @2016
const https = require("https");
const agent = new https.Agent({
  rejectUnauthorized: false
})
//fetch(myUrl, { agent })
require('ssl-root-cas').inject();
const fs = require('fs');
const fetch = require("node-fetch");
require('dotenv').config();
import { createToken } from '../jwtHelper'
const YOUR_DOMAIN = 'zhaiduo.com';
const backendPort = 8003;
const ucfirst = (str) => {
  return str.charAt(0).toUpperCase() + str.substr(1);
}

const url = fs.existsSync(`${process.env.ADM_SSL_FILE_DIR}${YOUR_DOMAIN}.pem`) ? `https://${YOUR_DOMAIN}` : 'http://localhost'
const opt = fs.existsSync(`${process.env.ADM_SSL_FILE_DIR}${YOUR_DOMAIN}.pem`) ? {
  agent
} : {}

const paddingZero = (n) => {
  if (parseInt(n, 10) < 10) {
    return '0' + n;
  } else {
    return n + '';
  }
}

const getDateObj = (t, returnString) => {
  let dt;
  if (t !== undefined) {
    if (typeof t === 'string' || typeof t === 'number') {
      dt = new Date(t);
    } else {
      dt = new Date();
    }
  } else {
    dt = new Date();
  }
  return returnString ? dt.getFullYear() + "-" + paddingZero(dt.getMonth() + 1) + "-" + paddingZero(dt.getDate()) : {
    y: dt.getFullYear(),
    m: paddingZero(dt.getMonth() + 1),
    d: paddingZero(dt.getDate()),
    h: paddingZero(dt.getHours()),
    i: paddingZero(dt.getMinutes()),
    s: paddingZero(dt.getSeconds())
  };
}

const getLocalTime = () => {
  let obj = getDateObj();
  return obj.y + "-" + obj.m + "-" + obj.d + " " + obj.h + ":" + obj.i + ":" + obj.s;
}

const tidyPostData = (data) => {
  let t = ''
  Object.keys(data).forEach(k => {
    let kk = null
    if (k.match(/^(.+):(.+)$/i)) {
      t = RegExp.$1 + ucfirst(RegExp.$2)
      data[t] = data[k]
      delete data[k]
      kk = t
    } else {
      kk = k
    }
    if (typeof data[kk] === 'object' && data[kk].hasOwnProperty('$t')) {
      data[kk]['_t'] = data[kk]['$t']
      delete data[kk]['$t']
    }
  })
  //if (i == 1) console.log("blogposts data", data)
  return data
}

let lastId = ''
export const Resolvers = {
  Query: {
    blogPosts(root, args, context) {
      // http://localhost:8003/post/?_start=1&_limit=10&_sort=wp:post_date&_order=DESC
      //return BlogPost.findOne({ where: {id}});
      //http://graphql.org/learn/pagination/
      const start = args.first || 1
      const limit = args.offset || 10
      //console.log('args', args, start, limit)

      return fetch(`${url}:${backendPort}/post/?_start=${start}&_limit=${limit}&_sort=wp:post_date&_order=DESC`, opt).then(res => res.json())
        .then(data => {
          data.map((item, i) => {
            if (i === 0)
              lastId = item.id
            return tidyPostData(item)
          })
          return data
        })
    },

  },
  Mutation: {
    async createBlogpost(_, {title, contentEncoded}) {
      let newId = lastId || ''
      if (!newId) {
        const qData = await fetch(`${url}:${backendPort}/post/?_start=0&_limit=1&_sort=wp:post_date&_order=DESC`, opt).then(res => res.json())
        //console.log("qData", qData)
        /*if (Array.isArray(qData)) qData.map((item, i) => {
            if (i === 0)
              lastId = parseInt(item.id, 10) + 1; //item.id
            console.log("lastId", lastId)
          })*/

        if (Array.isArray(qData))
          newId = parseInt(qData[0].id, 10) + 1;
      }
      const body = {
        "title": {
          "$t": title
        },
        "link": {
          "$t": `https://${YOUR_DOMAIN}/${newId}.html`
        },
        "pubDate": {
          "$t": new Date() //"Tue, 14 Nov 2006 16:29:48 +0000"
        },
        "dc:creator": {
          "$t": "admin"
        },
        "guid": {
          "isPermaLink": "false",
          "$t": `https://${YOUR_DOMAIN}/${newId}.html`
        },
        "description": {},
        "content:encoded": {
          "$t": contentEncoded
        },
        "excerpt:encoded": {},
        "wp:post_id": {
          "$t": newId
        },
        "wp:post_date": {
          "$t": getLocalTime() //"2006-11-15 00:29:48"
        },
        "wp:post_date_gmt": {
          "$t": getLocalTime() //"2006-11-14 16:29:48"
        },
        "wp:comment_status": {
          "$t": "open"
        },
        "wp:ping_status": {
          "$t": "open"
        },
        "wp:post_name": {
          "$t": "%e7%a7%91%e6%8a%80%e6%96%b0%e7%9f%a5"
        },
        "wp:status": {
          "$t": "publish"
        },
        "wp:post_parent": {
          "$t": "0"
        },
        "wp:menu_order": {
          "$t": "0"
        },
        "wp:post_type": {
          "$t": "page"
        },
        "wp:post_password": {},
        "wp:is_sticky": {
          "$t": "0"
        }
      }
      //should return error?
      if (!newId) return {}
      return fetch(`${url}:${backendPort}/post/`, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        //'Authorization': 'JWT ' + currentToken,
        },
      })
        .then(res => {
          let data = {}
          if (res.status === 200) {
            data = res.json()
          } else {
            data = Promise.reject('request failed')
          }
          return data
        })
        .then((res) => {
          console.log("post ", res)
          tidyPostData(body)
        })
        .catch(err => {
          console.log("post err", err)
        });

    },
    signInAuth(_, {account, password}, ctx) {
      const _account = account.replace(/[^0-9a-z_]/gi, "").substr(0, 20)
      const _password = password.replace(/[^0-9a-z_@\+\.\-]/gi, "").substr(0, 20)
      return fetch(`${url}:${backendPort}/auth/?user_like=${_account}$&passwd_like=${_password}$`, {
        //method: 'GET',
        //body: JSON.stringify(body),
        /*headers: {
          'Content-Type': 'application/json',
          //'Authorization': 'JWT ' + currentToken,

        },*/
      })
        .then(res => res.json())
        .then(auth => {
          let token = ''
          //console.log("signInAuth", auth, ctx)
          if (Array.isArray(auth) && auth[0] && auth[0].id) {
            //auth.jwt = createToken(auth.user);
            //auth.jwt = createToken(_account);
            token = createToken(auth[0].user)
            auth[0].jwt = token
            ctx.user = Promise.resolve(auth[0]);
            return {
              id: auth[0].id,
              jwt: token,
            };
          } else {
            return Promise.reject('password incorrect');
          }
        })
        .catch(err => {
          console.log("signInAuth err", err)
          return Promise.reject('password incorrect');
        });
    },
  },
};
export default Resolvers;


const https = require("https");
const agent = new https.Agent({
  rejectUnauthorized: false
})
//fetch(myUrl, { agent })
require('ssl-root-cas').inject();
const fs = require('fs');
const fetch = require("node-fetch");
require('dotenv').config();
import { createToken } from '../jwtHelper'
const YOUR_DOMAIN = 'zhaiduo.com';
const backendPort = 8003;
const ucfirst = (str) => {
  return str.charAt(0).toUpperCase() + str.substr(1);
}

const url = fs.existsSync(`${process.env.ADM_SSL_FILE_DIR}${YOUR_DOMAIN}.pem`) ? `https://${YOUR_DOMAIN}` : 'http://localhost'
const opt = fs.existsSync(`${process.env.ADM_SSL_FILE_DIR}${YOUR_DOMAIN}.pem`) ? {
  agent
} : {}

const paddingZero = (n) => {
  if (parseInt(n, 10) < 10) {
    return '0' + n;
  } else {
    return n + '';
  }
}

const getDateObj = (t, returnString) => {
  let dt;
  if (t !== undefined) {
    if (typeof t === 'string' || typeof t === 'number') {
      dt = new Date(t);
    } else {
      dt = new Date();
    }
  } else {
    dt = new Date();
  }
  return returnString ? dt.getFullYear() + "-" + paddingZero(dt.getMonth() + 1) + "-" + paddingZero(dt.getDate()) : {
    y: dt.getFullYear(),
    m: paddingZero(dt.getMonth() + 1),
    d: paddingZero(dt.getDate()),
    h: paddingZero(dt.getHours()),
    i: paddingZero(dt.getMinutes()),
    s: paddingZero(dt.getSeconds())
  };
}

const getLocalTime = () => {
  let obj = getDateObj();
  return obj.y + "-" + obj.m + "-" + obj.d + " " + obj.h + ":" + obj.i + ":" + obj.s;
}

const tidyPostData = (data) => {
  let t = ''
  Object.keys(data).forEach(k => {
    let kk = null
    if (k.match(/^(.+):(.+)$/i)) {
      t = RegExp.$1 + ucfirst(RegExp.$2)
      data[t] = data[k]
      delete data[k]
      kk = t
    } else {
      kk = k
    }
    if (typeof data[kk] === 'object' && data[kk].hasOwnProperty('$t')) {
      data[kk]['_t'] = data[kk]['$t']
      delete data[kk]['$t']
    }
  })
  //if (i == 1) console.log("blogposts data", data)
  return data
}

let lastId = ''
export const Resolvers = {
  Query: {
    blogPosts(root, args, context) {
      // http://localhost:8003/post/?_start=1&_limit=10&_sort=wp:post_date&_order=DESC
      //return BlogPost.findOne({ where: {id}});
      //http://graphql.org/learn/pagination/
      const start = args.first || 1
      const limit = args.offset || 10
      //console.log('args', args, start, limit)

      return fetch(`${url}:${backendPort}/post/?_start=${start}&_limit=${limit}&_sort=wp:post_date&_order=DESC`, opt).then(res => res.json())
        .then(data => {
          data.map((item, i) => {
            if (i === 0)
              lastId = item.id
            return tidyPostData(item)
          })
          return data
        })
    },

  },
  Mutation: {
    async createBlogpost(_, {title, contentEncoded}) {
      let newId = lastId || ''
      if (!newId) {
        const qData = await fetch(`${url}:${backendPort}/post/?_start=0&_limit=1&_sort=wp:post_date&_order=DESC`, opt).then(res => res.json())
        //console.log("qData", qData)
        /*if (Array.isArray(qData)) qData.map((item, i) => {
            if (i === 0)
              lastId = parseInt(item.id, 10) + 1; //item.id
            console.log("lastId", lastId)
          })*/

        if (Array.isArray(qData))
          newId = parseInt(qData[0].id, 10) + 1;
      }
      const body = {
        "title": {
          "$t": title
        },
        "link": {
          "$t": `https://${YOUR_DOMAIN}/${newId}.html`
        },
        "pubDate": {
          "$t": new Date() //"Tue, 14 Nov 2006 16:29:48 +0000"
        },
        "dc:creator": {
          "$t": "admin"
        },
        "guid": {
          "isPermaLink": "false",
          "$t": `https://${YOUR_DOMAIN}/${newId}.html`
        },
        "description": {},
        "content:encoded": {
          "$t": contentEncoded
        },
        "excerpt:encoded": {},
        "wp:post_id": {
          "$t": newId
        },
        "wp:post_date": {
          "$t": getLocalTime() //"2006-11-15 00:29:48"
        },
        "wp:post_date_gmt": {
          "$t": getLocalTime() //"2006-11-14 16:29:48"
        },
        "wp:comment_status": {
          "$t": "open"
        },
        "wp:ping_status": {
          "$t": "open"
        },
        "wp:post_name": {
          "$t": "%e7%a7%91%e6%8a%80%e6%96%b0%e7%9f%a5"
        },
        "wp:status": {
          "$t": "publish"
        },
        "wp:post_parent": {
          "$t": "0"
        },
        "wp:menu_order": {
          "$t": "0"
        },
        "wp:post_type": {
          "$t": "page"
        },
        "wp:post_password": {},
        "wp:is_sticky": {
          "$t": "0"
        }
      }
      //should return error?
      if (!newId) return {}
      return fetch(`${url}:${backendPort}/post/`, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        //'Authorization': 'JWT ' + currentToken,
        },
      })
        .then(res => {
          let data = {}
          if (res.status === 200) {
            data = res.json()
          } else {
            data = Promise.reject('request failed')
          }
          return data
        })
        .then((res) => {
          console.log("post ", res)
          tidyPostData(body)
        })
        .catch(err => {
          console.log("post err", err)
        });

    },
    signInAuth(_, {account, password}, ctx) {
      const _account = account.replace(/[^0-9a-z_]/gi, "").substr(0, 20)
      const _password = password.replace(/[^0-9a-z_@\+\.\-]/gi, "").substr(0, 20)
      return fetch(`${url}:${backendPort}/auth/?user_like=${_account}$&passwd_like=${_password}$`, {
        //method: 'GET',
        //body: JSON.stringify(body),
        /*headers: {
          'Content-Type': 'application/json',
          //'Authorization': 'JWT ' + currentToken,

        },*/
      })
        .then(res => res.json())
        .then(auth => {
          let token = ''
          //console.log("signInAuth", auth, ctx)
          if (Array.isArray(auth) && auth[0] && auth[0].id) {
            //auth.jwt = createToken(auth.user);
            //auth.jwt = createToken(_account);
            token = createToken(auth[0].user)
            auth[0].jwt = token
            ctx.user = Promise.resolve(auth[0]);
            return {
              id: auth[0].id,
              jwt: token,
            };
          } else {
            return Promise.reject('password incorrect');
          }
        })
        .catch(err => {
          console.log("signInAuth err", err)
          return Promise.reject('password incorrect');
        });
    },
  },
};
export default Resolvers;

