// server.js by zhaiduo@gmail.com @2016
const _ = require('lodash');
const fs = require('fs');
const https = require('https')
const jsonServer = require('json-server')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
require('dotenv').config();
//config
const frontendHost = 'http://localhost';
const backendHost = 'http://localhost';
const backendHostSSL = 'https://localhost';
const frontendPort = 3000;
const backendPort = 8003;
const SSL_FILE_DIR = process.env.ADM_SSL_FILE_DIR;
const YOUR_DOMAIN = 'zhaiduo.com';
const hostMatchReg = new RegExp("^(https?:\\/\\/)(localhost|zhaiduo\\.com)(:?)([0-9]+)?","i");
const localHostMatchReg = new RegExp("^localhost","i");
const dbName = 'blogs.json';
const db = `data/${dbName}`;
const bakdb = `adam/${dbName}`;
const adminName = process.env.ADM_USER;
const adminHash = process.env.ADM_PASS;

import { createToken, secret } from './jwtHelper'

//////////////GraphQL
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { 
  makeExecutableSchema, 
  //addMockFunctionsToSchema
 } from 'graphql-tools';
import { Schema } from './gqlData/schema';
//import { Mocks } from './gqlData/mocks';
import { Resolvers } from './gqlData/resolvers';

const executableSchema = makeExecutableSchema({
  typeDefs: Schema,
  resolvers: Resolvers,
});
/*addMockFunctionsToSchema({
  schema: executableSchema,
  mocks: Mocks,
  preserveResolvers: true,
});*/

//////////////////////

const paddingZero = n => {
  if (parseInt(n, 10) < 10) {
    return '0' + n;
  } else {
    return n + '';
  }
};
const getDateObj = (t, returnString) => {
  let dt;
  if (t !== undefined) {
    if (typeof t === 'string' || typeof t === 'number') {
      dt = new Date(t);
    } else {
      dt = new Date();
    }
  } else {
    dt = new Date();
  }
  return returnString ? dt.getFullYear() + "-" + paddingZero(dt.getMonth() + 1) + "-" + paddingZero(dt.getDate()) : {
    y: dt.getFullYear(),
    m: paddingZero(dt.getMonth() + 1),
    d: paddingZero(dt.getDate()),
    h: paddingZero(dt.getHours()),
    i: paddingZero(dt.getMinutes()),
    s: paddingZero(dt.getSeconds())
  };
};

const validToken = (req, res, secret) => {
  const host = allowedHost(req);
  //console.log("host", host)
  return new Promise((resolve, reject) => {
    let token;
    //console.log("req.headers.authorization", req.headers.authorization)
    if (req.headers && req.headers.authorization && req.headers.authorization.match(/^JWT (.+)$/i)) {
      token = RegExp.$1;
      jwt.verify(token, secret, function(err, decoded) {
        console.log('verify', err, decoded) // bar
        if (err) {
          //return res.sendStatus(401);
          reject(false);
        } else {
          res.setHeader('Access-Control-Allow-Credentials', 'true');
          res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
          res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          res.setHeader('Access-Control-Allow-Origin', host);
          //next()
          resolve(true);
        }
      });
    } else {
      //return res.sendStatus(401);
      reject(false);
    }
  })
}

const server = jsonServer.create()

//////////////GraphQL
server.use('/graphql', bodyParser.json(), graphqlExpress({
  schema: executableSchema,
  context: {}, // at least(!) an empty object
}));
server.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));
//////////////GraphQL

if (!fs.existsSync(db)) {
  console.log('init db')
  fs.createReadStream(bakdb).pipe(fs.createWriteStream(db));
}
const router = jsonServer.router(db)
const middlewares = jsonServer.defaults()

server.use(middlewares)
server.use(cookieParser())

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
server.use(bodyParser.urlencoded({
  extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
server.use(bodyParser.json());

const allowedHost = function(req) {
  let host = `${frontendHost}:${frontendPort}`;
  if (req.headers && req.headers.referer && req.headers.referer.toString().match(hostMatchReg)) {
    host = RegExp.$1 + RegExp.$2 + RegExp.$3 + RegExp.$4;
  }
  console.log('allowedHost', host)
  return host;
};

server.get('/get-token/', function(req, res) {
  console.log("get-token", req.method)
  const host = allowedHost(req);
  //res.setHeader('Authorization', 'Bearer ' + );
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Origin', host);
  res.status(200).send(JSON.stringify({
    token: '' //createToken()
  }))
})

server.post('/refresh-token/', function(req, res) {
  console.log("refresh-token", req.method)
  let host = allowedHost(req);
  //console.log('req', req)
  //res.setHeader('Authorization', 'Bearer ' + );
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Origin', host);

  validToken(req, res, secret).then(function() {
    return res.status(200).send(JSON.stringify({
      token: createToken()
    }));
  //next()
  }).catch(function() {
    return res.sendStatus(401);
  });
  /*
    Example:
      Access-Control-Allow-Credentials:true
      Access-Control-Allow-Headers:X-Requested-With,content-type
      Access-Control-Allow-Methods:GET, POST, OPTIONS, PUT, PATCH, DELETE
      Access-Control-Allow-Origin:http://localhost:8088
  */

})

server.post('/auth/', function(req, res) {
  console.log("auth", req.body)
  let host = allowedHost(req);
  console.log('req', req)
  //res.setHeader('Authorization', 'Bearer ' + );
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Origin', host);

  if (req.body.hasOwnProperty('username') && req.body.hasOwnProperty('passwd')) {
    if (req.body.username === adminName && req.body.passwd === adminHash) {
      res.status(200).send(JSON.stringify({
        token: createToken()
      }))
    } else {
      res.status(401).send(err.name ? err.name : "UnauthorizedError ")
    }
  } else {
    res.status(401).send(err.name ? err.name : "UnauthorizedError ")
  }

})

server.use(function(err, req, res, next) {
  if (!err) return next(); // you also need this line
  //console.log("error!!!", err, req);
  res.status(401).send(err.name ? err.name : "UnauthorizedError ")
});

/*
Example:
    GET    /posts
    GET    /posts/1
    POST   /posts
    PUT    /posts/1
    PATCH  /posts/1
    DELETE /posts/1

http://localhost:8003
http://localhost:8003/post/?_start=1&_limit=10&_sort=wp:post_date&_order=DESC
*/

server.use(function(req, res, next) {
  //console.log("req.user", req.method)
  if (req.method && req.method.match(/^get$/i)) {
    if (req._parsedUrl.pathname === '/auth/') {
      //access only for localhost
      if (req.headers.host.match(localHostMatchReg)) {
        next()
      } else {
        return res.sendStatus(401);
      }
    } else {
      next()
    }
  } else {
    if (req._parsedUrl.pathname === '/auth/') {
      if (req.method.match(/^post$/i)) {
        //only localhost can post to API
        if (!req.headers.host.match(localHostMatchReg)) {
          return res.sendStatus(401);
        }
      } else {
        return res.sendStatus(401);
      }
    }
    validToken(req, res, secret).then(function() {
      //let dt, bakname;
      //return res.sendStatus(401);
      //backup automatically
      /*if (fs.existsSync('data/test_converted.json')) {
        dt = getDateObj();
        bakname = dt.y + dt.m;
        console.log('bak db: ' + bakname)
        fs.createReadStream('data/test_converted.json').pipe(fs.createWriteStream('adam/test_converted_bak_' + bakname + '.json'));
      }*/
      next()
    }).catch(function() {
      return res.sendStatus(401);
    });

  }
})

server.use(router)

if (fs.existsSync(`${SSL_FILE_DIR}${YOUR_DOMAIN}.pem`)) {
  const options = {
    //key: fs.readFileSync('./ssl/key.pem'),
    key: fs.readFileSync(`${SSL_FILE_DIR}${YOUR_DOMAIN}.pem`),
    //cert: fs.readFileSync('./ssl/cert.pem')
    cert: fs.readFileSync(`${SSL_FILE_DIR}1_${YOUR_DOMAIN}_bundle.pem`)
  };
  https.createServer(options, server).listen(backendPort, function() {
    console.log(`json-server started on ${backendHostSSL}:${backendPort}`);
  });
} else {
  server.listen(backendPort, function() {
    console.log(`JSON Server is running on ${backendHost}:${backendPort}`)
  })
}

