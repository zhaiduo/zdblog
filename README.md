# Zdblog

Blog Tool based on React 15.1.0 and Puppeteer 0.12.0.

## About
This blog tool is based on my own blog system about two years ago (Mon Apr 25 00:38:21 2016 +0800).
The main purpose is to publish personal blogs to website easily and fast, no online backend needed. Although the source seems too mess and old, I plan to update it continuously. Currently this tool helps me to publish blog to my own blog: https://zhaiduo.com. Wish I can optimize it constantly, even help someone.

## Structure
This tool is based on two parts: frontend and backend.
The front part is based on react/redux, the back part is based on express/graphql, ths data base is json file.

## How to install backend?
Check it out: [INSTALL.md](INSTALL.md)

